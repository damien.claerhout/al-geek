package fr.afcepf.al34.algeek.testsSelenium;


import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SeleniumAppTests {


	@Test
	public void testInutile1() {
		assertTrue(true);
	}

	@Test
	public void testInutile2() {
		assertTrue(true);
	}
	
	@Test
	public void testInutile3() {
		assertTrue(true);
	}
	
	@Ignore ("Test selenium désactivé pour déploiement sur AWS")
	@Test
	public void testEffectuerUneCommande() throws InterruptedException {
		
		String chromeDriverPath ="C:\\Drivers\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", chromeDriverPath);
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized"); // open Browser in maximized mode
		
		WebDriver driver = new ChromeDriver(options);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		Thread.sleep(250);

		try {
			// connexion au site AL Geek
			driver.navigate().to("http://localhost:8080/projetAL");
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.MINUTES);
			//		Thread.sleep(1000);
			if(driver.getTitle().equalsIgnoreCase("Home | AL-Geek")) {
				System.out.println("Le titre contient Home | AL-Geek");
			} else {
				System.out.println("La page Home | AL-Geek est introuvable");
			}

			// Connexion en tant que Arthur Dupont
			Thread.sleep(800); 
			WebElement lienConnexion = driver.findElement(By.id("j_idt14:cnnx"));//clique ici sur un lien pour ouvrir une nouvelle fenetre.
			lienConnexion.click();
			Thread.sleep(800);        
			WebElement login = driver.findElement(By.id("j_idt14:loginmail"));
			login.sendKeys("adupont@gmail.com");
			Thread.sleep(400); 
			WebElement password = driver.findElement(By.id("j_idt14:password"));
			password.sendKeys("azerty") ;
			Thread.sleep(800);        
			WebElement btnCnx = driver.findElement(By.id("j_idt14:connexOK"));
			btnCnx.click();
			Thread.sleep(800);

			// Achat ordinateur custom avec configurateur
			driver.navigate().to("http://localhost:8080/projetAL/configurateur.xhtml");
			Thread.sleep(800);
			WebElement processeur = driver.findElement(By.id("j_idt64:j_idt67:0:selectCmpt"));
			processeur.click();
			WebElement processeur2 = driver.findElement(By.id("j_idt64:j_idt114:1:selectOK"));			
			processeur2.click();
			jse.executeScript("window.scrollBy(0, 300)");
			Thread.sleep(300); 
			
			WebElement ventirad = driver.findElement(By.id("j_idt64:j_idt67:1:selectCmpt"));
			ventirad.click();
			WebElement ventirad2 = driver.findElement(By.id("j_idt64:j_idt114:3:selectOK"));			
			ventirad2.click();
			jse.executeScript("window.scrollBy(0, 400)");
			Thread.sleep(300); 
			
			WebElement cartemere = driver.findElement(By.id("j_idt64:j_idt67:2:selectCmpt"));
			cartemere.click();
			WebElement cartemere2 = driver.findElement(By.id("j_idt64:j_idt114:1:selectOK"));			
			cartemere2.click();
			jse.executeScript("window.scrollBy(0, 500)");
			Thread.sleep(300); 
			
			WebElement ram = driver.findElement(By.id("j_idt64:j_idt67:3:selectCmpt"));
			ram.click();
			WebElement ram2 = driver.findElement(By.id("j_idt64:j_idt114:0:selectOK"));			
			ram2.click();
			jse.executeScript("window.scrollBy(0, 600)");
			Thread.sleep(300); 
			
			WebElement ssd = driver.findElement(By.id("j_idt64:j_idt67:4:selectCmpt"));
			ssd.click();
			WebElement ssd2 = driver.findElement(By.id("j_idt64:j_idt114:0:selectOK"));			
			ssd2.click();
			jse.executeScript("window.scrollBy(0, 700)");
			
			WebElement carteGraphique = driver.findElement(By.id("j_idt64:j_idt67:5:selectCmpt"));
			carteGraphique.click();
			WebElement carteGraphique2 = driver.findElement(By.id("j_idt64:j_idt114:0:selectOK"));			
			carteGraphique2.click();
			jse.executeScript("window.scrollBy(0, 800)");
			Thread.sleep(300); 
			
			WebElement boitier = driver.findElement(By.id("j_idt64:j_idt67:6:selectCmpt"));
			boitier.click();
			WebElement boitier2 = driver.findElement(By.id("j_idt64:j_idt114:0:selectOK"));			
			boitier2.click();
			jse.executeScript("window.scrollBy(0, 9000)");
			Thread.sleep(300); 
			
			WebElement alim = driver.findElement(By.id("j_idt64:j_idt67:7:selectCmpt"));
			alim.click();
			WebElement alim2 = driver.findElement(By.id("j_idt64:j_idt114:0:selectOK"));			
			alim2.click();
			jse.executeScript("window.scrollBy(0, 1000)");
			Thread.sleep(300); 
			
			WebElement carteReseau = driver.findElement(By.id("j_idt64:j_idt67:8:selectCmpt"));
			carteReseau.click();
			WebElement carteReseau2 = driver.findElement(By.id("j_idt64:j_idt114:0:selectOK"));			
			carteReseau2.click();
			jse.executeScript("window.scrollBy(0, 1100)");
			Thread.sleep(750);

			jse.executeScript("window.scrollBy(0,-250)","");
			Thread.sleep(50); 
			jse.executeScript("window.scrollBy(0,-250)","");
			Thread.sleep(500); 
			jse.executeScript("window.scrollBy(0,-250)","");		
			Thread.sleep(500); 
			WebElement ajoutPanier = driver.findElement(By.id("j_idt64:btnAjoutPanier"));  
			ajoutPanier.click();

			// Consultation puis validation panier
			Thread.sleep(800);  
			jse.executeScript("window.scrollBy(0,250)");
			Thread.sleep(600);
			jse.executeScript("window.scrollBy(0,250)");
			Thread.sleep(600);
			jse.executeScript("window.scrollBy(0,250)");
			Thread.sleep(600);
			WebElement finalize = driver.findElement(By.id("form1:finalize"));
			finalize.click();

			// Paiement 
			Thread.sleep(800);;
			jse.executeScript("window.scrollBy(0,500)");
			Thread.sleep(600);
			jse.executeScript("window.scrollBy(0,500)");
			Thread.sleep(600);
			jse.executeScript("window.scrollBy(0,500)");
			Thread.sleep(600);
			WebElement democb = driver.findElement(By.id("form3:btncbdemo"));
			democb.click();
			Thread.sleep(600);
			WebElement btnvalid = driver.findElement(By.id("form4:btnvalid"));
			btnvalid.click();
			
			//Telechargement Facture
			WebElement btnfacture = driver.findElement(By.id("j_idt62:btntelech"));
			btnfacture.click();
			Thread.sleep(10000);
			
		} finally {
			driver.quit();			
		}
	}

}



