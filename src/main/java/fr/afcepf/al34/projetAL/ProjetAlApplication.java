package fr.afcepf.al34.projetAL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;



@SpringBootApplication
public class ProjetAlApplication implements WebMvcConfigurer{
	
	  @Override
	  public void addViewControllers(ViewControllerRegistry registry) {
	    registry.addViewController("/")
	    	.setViewName("forward:/home.xhtml");
	   // registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
	  }

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(ProjetAlApplication.class);
		app.setAdditionalProfiles("initData");
		

		ConfigurableApplicationContext context = app.run(args);


		System.out.println("http://localhost:8080/projetAL");

	}
	

    @SuppressWarnings("deprecation")
	@Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");
            }
        };
    }

}
