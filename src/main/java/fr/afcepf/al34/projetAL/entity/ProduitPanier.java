package fr.afcepf.al34.projetAL.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @ToString()
public class ProduitPanier {

	private Produit produit;
	
	private Panier panier;
	
	
	public ProduitPanier(Panier panier, Produit produit) {
		super();
		this.panier = panier;
		this.produit = produit;
	}


}
