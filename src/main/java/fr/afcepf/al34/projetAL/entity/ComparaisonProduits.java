package fr.afcepf.al34.projetAL.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class ComparaisonProduits {
	
private Produit premierProduit;
private Produit secondProduit;


	public ComparaisonProduits(Produit premierProduit, Produit secondProduit) {
		super();
		this.premierProduit = premierProduit;
		this.secondProduit = secondProduit;
	}
		
}
