package fr.afcepf.al34.projetAL.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Client implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="nom")
	private String nom;
	
	@Column(name="prenom")
	private String prenom;
	
	@Column(name="email")
	private String email;
	
	@Column(name="hashed_password", length=5000)
	private String hashedPassword;
	
	@Column(name="salt")
	private String salt;
	
	@Column(name="adresse")
	private String adresse;
	
	@Column(name = "codePostal")
	private String codePostal;
	
	@Column(name = "ville")
	private String ville;
	
	@Column(name="numero_fixe")
	private String numeroFixe;
	
	@Column(name="numeroPort")
	private String numeroPort;
	
	
	public Client(String nom, String prenom, 
				String email, String login) {
	this.nom = nom;
	this.prenom = prenom;
	this.email = email;
	}
	

	public String getVille() {
		return ville;
	}



	public void setVille(String ville) {
		this.ville = ville;
	}



	public Client(String nom, String prenom, String email, String login, String hashedPassword, String salt,
			String adresse, String codePostal, String ville) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.hashedPassword = hashedPassword;
		this.salt = salt;
		this.adresse = adresse;
		this.codePostal = codePostal;
		this.ville = ville;
	}


	public Client() {
	}



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	
	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public String getHashedPassword() {
		return hashedPassword;
	}
	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}


	public String getNumeroFixe() {
		return numeroFixe;
	}


	public void setNumeroFixe(String numeroFixe) {
		this.numeroFixe = numeroFixe;
	}


	public String getNumeroPort() {
		return numeroPort;
	}


	public void setNumeroPort(String numeroPort) {
		this.numeroPort = numeroPort;
	}

	
}
