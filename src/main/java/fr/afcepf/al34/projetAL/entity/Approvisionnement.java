package fr.afcepf.al34.projetAL.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import fr.afcepf.al34.projetAL.entity.Produit;


@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id", scope = Approvisionnement.class)
public class Approvisionnement implements Serializable{
	
	/**
	 *  
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
		
	@ManyToOne
	@JoinColumn(referencedColumnName="id")
	private Fournisseur fournisseur;
	
	@Column(name="quantite_commandee")
	private int quantiteCommandee;
		
	@Column(name="prix")
	private double prix;
	
	@Column(name="date_approvisionnement")
	private Date dateApprovisionnement;
		
	@ManyToOne
	@JoinColumn(referencedColumnName="id")
	private Stock stock;

	
	
	
	public Approvisionnement() {
		super();
	}


	public Approvisionnement(Integer id, Produit produit, Fournisseur fournisseur, int quantiteCommandee, double prix,
			Date dateApprovisionnement, Stock stock) {
		super();
		this.id = id;
		this.fournisseur = fournisseur;
		this.quantiteCommandee = quantiteCommandee;
		this.prix = prix;
		this.dateApprovisionnement = dateApprovisionnement;
		this.stock = stock;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}



	public Fournisseur getFournisseur() {
		return fournisseur;
	}


	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}


	public int getQuantiteCommandee() {
		return quantiteCommandee;
	}


	public void setQuantiteCommandee(int quantiteCommandee) {
		this.quantiteCommandee = quantiteCommandee;
	}


	public double getPrix() {
		return prix;
	}


	public void setPrix(double prix) {
		this.prix = prix;
	}


	public Date getDateApprovisionnement() {
		return dateApprovisionnement;
	}


	public void setDateApprovisionnement(Date dateApprovisionnement) {
		this.dateApprovisionnement = dateApprovisionnement;
	}


	public Stock getStock() {
		return stock;
	}


	public void setStock(Stock stock) {
		this.stock = stock;
	}
	


	
}
