package fr.afcepf.al34.projetAL.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class ChoixComposant {
	
	private TypeProduit typeProduit;
	
	private boolean obligatoire;
	
	private Produit produit;
	
	private String message;
	
	public ChoixComposant(TypeProduit type) {
		typeProduit = type;
	}
}
