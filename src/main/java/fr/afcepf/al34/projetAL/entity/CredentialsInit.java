package fr.afcepf.al34.projetAL.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @ToString()
public class CredentialsInit {

	private Credentials credentials;
	
	private String clearPassword;

	public CredentialsInit(Credentials credentials, String clearPassword) {
		super();
		this.credentials = credentials;
		this.clearPassword = clearPassword;
	}
	
	
}
