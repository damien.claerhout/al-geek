package fr.afcepf.al34.projetAL.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


import fr.afcepf.al34.projetAL.entity.Produit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="stock")
@Getter @Setter @NoArgsConstructor @ToString()
public class Stock implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable=false)
	private Long id;
	
	@OneToOne
	@JoinColumn(referencedColumnName="id")
	private Produit produit;
	
	@Column(name="quantite_dispo_site_internet")
	private int quantiteDispoSiteInternet;
	
	@Column(name="quantite_dispo_physique")
	private int quantiteDispoPhysique;
		

	
	//Constructeur chargé

	public Stock(Long id, Produit produit, int quantiteDispoSiteInternet, int quantiteDispoPhysique) {
		super();
		this.id = id;
		this.produit = produit;
		this.quantiteDispoSiteInternet = quantiteDispoSiteInternet;
		this.quantiteDispoPhysique = quantiteDispoPhysique;
	}

}
