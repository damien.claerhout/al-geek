package fr.afcepf.al34.projetAL.entity.composant;

import javax.persistence.Column;

import fr.afcepf.al34.projetAL.entity.Caracteristique;
import fr.afcepf.al34.projetAL.entity.Produit;

public abstract class ComposantOrdinateur {

	@Column(name="produit")
	private Produit produit;
	
	protected ComposantOrdinateur(Produit p) {
		produit = p;
	}
	
	public Object getCaracteristique(String cle) {
		Caracteristique c = produit.getAttribute(cle);
		
		return c == null ? null : c.getValeurAuto();
	}

	public void setCaracteristique(String cle, Object val) {
		Caracteristique c = produit.getAttribute(cle);
		
		if (c != null)
			c.setValeurAuto(val);
	}

	public Produit getProduit() {
		return produit;
	}

	public void setProduit(Produit produit) {
		this.produit = produit;
	}

	public abstract boolean estCompatibleAvec(ComposantOrdinateur autre);
	
}
