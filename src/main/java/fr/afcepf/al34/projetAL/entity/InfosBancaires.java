package fr.afcepf.al34.projetAL.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
public class InfosBancaires implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter
	private Long id;
	
	@Getter @Setter
	@Column(name="type_carte")
	private String typeCarte;
	
	@Getter @Setter
	@Column(name="numero_carte")
	//@Size(min=16, max=16, message = "Le n° de carte bancaire est composé 16 chiffres")
	private String numCarte;
	
	@Getter @Setter
	@Column(name = "date_expiration")
	private Date dateCarte;
	
	@Getter @Setter
	@Column(name = "cryptogramme")
	//@Size(min=3, max=3, message = "Le cryptogramme est composé de 3 chiffres")
	private String cryptogramme;
	



	public InfosBancaires() {
		super();
	}
	
	
	
	
}
