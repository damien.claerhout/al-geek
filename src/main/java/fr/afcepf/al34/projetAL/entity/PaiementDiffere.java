package fr.afcepf.al34.projetAL.entity;

import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import javax.persistence.OneToOne;


@Entity
public class PaiementDiffere implements Serializable{
	
	private static final long serialVersionUID = 1L;
	


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="somme")
	private double somme;
	
	@Column(name="mois")
	private int mois;
	
	
	@Column(name="mensualite")
	private double mensualite;
	
	@ManyToOne
	private Client client;
	
	@OneToOne
	private Commande commande;
	
	
	
	public PaiementDiffere() {
		super();

	}


	public PaiementDiffere(double somme, int mois, double mensualite, Client client, Commande commande) {
		super();
		this.somme = somme;
		this.mois = mois;
		this.mensualite = mensualite;
		this.client = client;
		this.commande = commande;
	}

	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public double getSomme() {
		return somme;
	}


	public void setSomme(double somme) {
		this.somme = somme;
	}


	public int getMois() {
		return mois;
	}


	public void setMois(int mois) {
		this.mois = mois;
	}


	public double getMensualite() {
		return mensualite;
	}


	public void setMensualité(double mensualite) {
		this.mensualite = mensualite;
	}


	public Client getClient() {
		return client;
	}


	public void setClient(Client client) {
		this.client = client;
	}


	public Commande getCommande() {
		return commande;
	}


	public void setCommande(Commande commande) {
		this.commande = commande;
	}


	@Override
	public String toString() {
		return "PaiementDiffere [id=" + id + ", somme=" + somme + ", mois=" + mois + ", mensualite=" + mensualite
				+ ", client=" + client + ", commande=" + commande + "]";
	}



}
