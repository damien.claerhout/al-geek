package fr.afcepf.al34.projetAL.entity;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class Panier {

	@Getter @Setter
	private Client client;
	
	@Getter @Setter
	private List<LigneCommande> listLC;
	

	public Panier(Client client, List<LigneCommande> listLC) {
		super();
		this.client = client;
		this.listLC = new ArrayList<LigneCommande>();
	}

	public Panier() {
		super();
	}
	
	
}
