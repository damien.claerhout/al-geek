package fr.afcepf.al34.projetAL.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;

@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class,property="@id", scope = Commande.class)
public class Commande implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter
	private Long id;
	
	@Getter @Setter
	@OneToMany(fetch=FetchType.LAZY, mappedBy="commande", cascade=CascadeType.PERSIST)
	private List<LigneCommande> listLigneCommande;
	
	@Getter @Setter
	@Column(name="date_creation")
	@Temporal(TemporalType.DATE)
	private Date dateCreation;
	
	@Getter @Setter
	@Column(name="date_expedition")
	@Temporal(TemporalType.DATE)
	private Date dateExpedition;
	
	@Getter @Setter
	@ManyToOne
	private Client client;
	
	@OneToOne
	@Getter @Setter
	private InfosBancaires infosBank;
	
	@Getter @Setter
	private Double prix;
	
	public Commande() {
		super();
	}


	public Commande(Long id, List<LigneCommande> listLigneCommande, Date dateCreation, Double prix) {
		super();
		this.id = id;
		this.listLigneCommande = listLigneCommande;
		this.dateCreation = dateCreation;
		this.prix = prix;
	}


}
