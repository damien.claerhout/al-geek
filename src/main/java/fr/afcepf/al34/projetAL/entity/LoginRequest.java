package fr.afcepf.al34.projetAL.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @ToString
public class LoginRequest {
	
	private String username;
	private String password;
	
	
	public LoginRequest(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

}
