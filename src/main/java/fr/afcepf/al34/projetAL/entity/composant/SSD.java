package fr.afcepf.al34.projetAL.entity.composant;

import fr.afcepf.al34.projetAL.entity.Produit;

public class SSD extends ComposantOrdinateur {

	public static final String capaciteKey = "ssd_capacite";

	public SSD(Produit p) {
		super(p);
	}

	public Integer getCapacite() {
		return (Integer) getCaracteristique(capaciteKey);
	}

	public void setCapacite(Integer value) {
		setCaracteristique(capaciteKey, value);
	}

	@Override
	public boolean estCompatibleAvec(ComposantOrdinateur autre) {
		return true;
	}
}