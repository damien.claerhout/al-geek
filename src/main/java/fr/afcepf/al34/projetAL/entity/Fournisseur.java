package fr.afcepf.al34.projetAL.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Entity
@Table(name = "fournisseur")
@Getter @Setter @NoArgsConstructor @ToString()
public class Fournisseur implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable=false)
	private Long id;
	
	@Column(name="nom")
	private String nom;
		
	@Column(name="adresse")
	private String adresse;
	
	@Column(name="complement_adresse")
	private String complementAdresse;
	
	@ManyToOne
	@JoinColumn(referencedColumnName="id")
	private Ville ville;
	
	@Column(name="numero_fixe")
	private String numeroFixe;
	
	@Column(name="numero_port")
	private String numeroPort;
	
	@Column(name="email")
	private String email;
	

	//Constructeur chargé
	
	public Fournisseur(Long id, String nom, String adresse, String complementAdresse, Ville ville, String numeroFixe,
			String numeroPort, String email) {
		super();
		this.id = id;
		this.nom = nom;
		this.adresse = adresse;
		this.complementAdresse = complementAdresse;
		this.ville = ville;
		this.numeroFixe = numeroFixe;
		this.numeroPort = numeroPort;
		this.email = email;
	}



	//Hashcode and equals
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adresse == null) ? 0 : adresse.hashCode());
		result = prime * result + ((complementAdresse == null) ? 0 : complementAdresse.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((numeroFixe == null) ? 0 : numeroFixe.hashCode());
		result = prime * result + ((numeroPort == null) ? 0 : numeroPort.hashCode());
		result = prime * result + ((ville == null) ? 0 : ville.hashCode());
		return result;
	}


	
}
