package fr.afcepf.al34.projetAL.service;

import fr.afcepf.al34.projetAL.entity.Panier;
import fr.afcepf.al34.projetAL.entity.Produit;

public interface PanierService {
	
	public Panier ajoutPanier(Panier panier, Produit produit);

	public Panier retirerProduitDuPanier(Panier panier, Produit produit);
}
