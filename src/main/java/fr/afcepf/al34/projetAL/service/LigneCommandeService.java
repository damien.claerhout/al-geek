package fr.afcepf.al34.projetAL.service;

import java.util.List;

import fr.afcepf.al34.projetAL.entity.LigneCommande;

public interface LigneCommandeService extends GenericService<LigneCommande> {
	
	public List<LigneCommande> getByCommande(Long commandeId);

}
