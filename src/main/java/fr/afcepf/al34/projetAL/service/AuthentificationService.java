package fr.afcepf.al34.projetAL.service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.projetAL.entity.Credentials;
import fr.afcepf.al34.projetAL.service.AuthentificationService;
import fr.afcepf.al34.projetAL.service.authentification.exception.AuthentificationException;


@Component
@Transactional
public class AuthentificationService {
	private static final int SALT_LENGTH = 20;

	private static Logger log = LogManager.getLogger();


	private String createSalt() {
		
		String chars = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder sb = new StringBuilder(chars.length());
		for (int i = 0 ; i < SALT_LENGTH ; i++) {
			int x = (int) (Math.random() * chars.length());
			sb.append(chars.charAt(x));
		}
		log.info("Salt créé avec succès");
		return sb.toString();
	}



	private String generateHash(String toHash) throws AuthentificationException {
		
		String result = "";
		
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte [] hashInBytes = md.digest(toHash.getBytes(StandardCharsets.UTF_8));
			StringBuilder sb = new StringBuilder();
			for (byte b : hashInBytes) {
				result += sb.append(String.format("%02x", b));
			}
			return result;
			
		} catch (NoSuchAlgorithmException e) {
			throw new AuthentificationException("Problème de connexion", e, null);
		}
	}



	public void initializeCredentials(Credentials cred, String clearPassword) throws AuthentificationException {
		
		String salt = createSalt();
		cred.setSalt(salt);
		String saltedPassword = clearPassword + salt;
		String hashedPassword = generateHash(saltedPassword);
		cred.setHashedPassword(hashedPassword);
		log.info("Credentials initié avec succès");
		
	}



	public boolean authentificate(Credentials cred, String clearPassword) throws AuthentificationException {
		String verif = generateHash(clearPassword + cred.getSalt());
		Boolean success = verif.equals(cred.getHashedPassword());
		log.info(cred.getLogin() + " s'est connecté avec succès");
		if(!success) {
			throw new AuthentificationException("Failure", null, cred.getLogin());
		} 
		return success;

	}

}
