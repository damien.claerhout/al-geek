package fr.afcepf.al34.projetAL.service;

import fr.afcepf.al34.projetAL.entity.Categorie;

public interface CategorieService extends GenericService<Categorie> {
	
	Categorie getRootCategorie();
	
}
