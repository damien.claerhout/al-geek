package fr.afcepf.al34.projetAL.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.projetAL.dao.CategorieDao;
import fr.afcepf.al34.projetAL.entity.Categorie;

@Component
@Transactional
public class CategorieServiceImpl extends GenericServiceImpl<Categorie> implements CategorieService {

	@Autowired
	private CategorieDao dao;

	@Override
	public CrudRepository<Categorie, Long> getDao() {
		return dao;
	}

	@Override
	public Categorie getRootCategorie() {
		return dao.findFirstByParentIsNull();
	}

}
