package fr.afcepf.al34.projetAL.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.projetAL.dao.ClientDao;
import fr.afcepf.al34.projetAL.entity.Client;
import fr.afcepf.al34.projetAL.entity.Credentials;
import fr.afcepf.al34.projetAL.service.authentification.exception.AuthentificationException;

@Component
@Transactional
public class ClientServiceImpl extends GenericServiceImpl<Client> implements ClientService {

	@Autowired
	private ClientDao dao;
	
	@Autowired
	private AuthentificationService authentificationService;
	
	@Override
	public CrudRepository<Client, Long> getDao() {
		return dao;
	}

	@Override
	public Client doConnecter(String email, String password) {
		Client c = dao.findByEmail(email);

		if (c == null)
			return null;

		Credentials cred = new Credentials();

		cred.setSalt(c.getSalt());
		cred.setHashedPassword(c.getHashedPassword());
		cred.setLogin(c.getEmail());

		if (c != null) {
			try {
				if (authentificationService.authentificate(cred, password)) {
					return c;
				}
			} catch (AuthentificationException e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	@Override
	public Client findByEmail(String email) {
		
		
		return dao.findByEmail(email);
	}


}
