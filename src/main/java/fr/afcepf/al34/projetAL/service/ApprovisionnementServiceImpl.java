package fr.afcepf.al34.projetAL.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.projetAL.dao.ApprovisionnementDao;
import fr.afcepf.al34.projetAL.entity.Approvisionnement;
import fr.afcepf.al34.projetAL.entity.Fournisseur;
import fr.afcepf.al34.projetAL.entity.Stock;



@Component
@Transactional
public class ApprovisionnementServiceImpl extends GenericServiceImpl<Approvisionnement> implements ApprovisionnementService {

	@Autowired
	private ApprovisionnementDao dao;
	
	
	@Override
	public CrudRepository<Approvisionnement, Long> getDao() {
		return dao;
	}



	@Override
	public List<Approvisionnement> findByStock(Stock stock) {
		
		return dao.findByStock(stock);
	}
	
	@Override
	public List<Approvisionnement> findByFournisseur(Fournisseur fournisseur) {
		
		return dao.findByFournisseur(fournisseur);
	}





}
