package fr.afcepf.al34.projetAL.service;

import java.util.List;

import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.entity.Stock;

public interface StockService extends GenericService<Stock> {
	
	
	public Stock getByProduit(Produit produit);
	
	public List<Stock> getByTypeProduit (Long typeProduitId);
	
	public List<Stock> getByQuantiteDispo(int quantite);
	
	
}
