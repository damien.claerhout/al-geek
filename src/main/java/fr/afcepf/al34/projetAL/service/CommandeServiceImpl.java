package fr.afcepf.al34.projetAL.service;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.projetAL.dao.CommandeDao;
import fr.afcepf.al34.projetAL.entity.Commande;
import fr.afcepf.al34.projetAL.entity.LigneCommande;
import fr.afcepf.al34.projetAL.entity.Stock;

@Component
@Transactional
public class CommandeServiceImpl extends GenericServiceImpl<Commande> implements CommandeService {

	@Autowired
	private CommandeDao dao;
	
	@Autowired
	private LigneCommandeService ligneCommandeService;
	
	@Autowired
	private StockService stockService;
	
	private SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yy");
	
	@Override
	public CrudRepository<Commande, Long> getDao() {
		return dao;
	}

	@Override
	public List<Commande> getCommandesPourClient(Long idClient) {
		return dao.findAllByClientIdOrderByDateCreationDesc(idClient);
	}

	@Override
	public List<Commande> getCommandesAtraiter() {
		return dao.findByDateExpeditionIsNull();
	}

	@Override
	public List<Commande> findByDateCreation(Date date) {
		formater.format(date);
		return dao.findByDateCreation(date);
	}

	@Override
	public Commande validerCommande(Commande commande) {
		
		DecimalFormat twoDForm =new DecimalFormat("##.##");
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator('.');
		twoDForm.setDecimalFormatSymbols(dfs);
		commande.setPrix(Double.parseDouble(twoDForm.format(commande.getPrix())));
		
		dao.save(commande);
		
		double prixTotalLigneCommande;		
		for (LigneCommande lc : commande.getListLigneCommande()) {
			lc.setCommande(commande);
			prixTotalLigneCommande = lc.getProduit().getPrix()*lc.getQuantite();
			prixTotalLigneCommande = Double.parseDouble(twoDForm.format(prixTotalLigneCommande));
			lc.setPrixTotal(prixTotalLigneCommande);
			lc=ligneCommandeService.ajouter(lc);
			Stock stock = stockService.getByProduit(lc.getProduit());
			stock.setQuantiteDispoSiteInternet(stock.getQuantiteDispoSiteInternet() - lc.getQuantite());
			stockService.modifier(stock);
		}
		
		return commande;
	}

}
