package fr.afcepf.al34.projetAL.service.utils;

import java.util.Comparator;

import fr.afcepf.al34.projetAL.entity.Produit;

public class TriMarqueDecroissant implements Comparator<Produit> {

	@Override
	public int compare(Produit o1, Produit o2) {
		return o2.getMarque().getNom().compareToIgnoreCase(o1.getMarque().getNom());
	}

}
