package fr.afcepf.al34.projetAL.service.utils;

import java.util.Comparator;

import fr.afcepf.al34.projetAL.entity.Produit;

public class TriPrixDecroissant implements Comparator<Produit> {

	@Override
	public int compare(Produit o1, Produit o2) {
		Double o1p = o1.getPrix();
		Double o2p = o2.getPrix();
		return o2p.compareTo(o1p);
		//return o2.getPrix().compareTo(o1.getPrix());
	}

}
