package fr.afcepf.al34.projetAL.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.projetAL.dao.ProduitDao;
import fr.afcepf.al34.projetAL.dao.TypeProduitDao;
import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.entity.TypeProduit;
import fr.afcepf.al34.projetAL.entity.composant.Alimentation;
import fr.afcepf.al34.projetAL.entity.composant.Boitier;
import fr.afcepf.al34.projetAL.entity.composant.CarteGraphique;
import fr.afcepf.al34.projetAL.entity.composant.CarteMere;
import fr.afcepf.al34.projetAL.entity.composant.CarteReseau;
import fr.afcepf.al34.projetAL.entity.composant.ComposantOrdinateur;
import fr.afcepf.al34.projetAL.entity.composant.Processeur;
import fr.afcepf.al34.projetAL.entity.composant.RAM;
import fr.afcepf.al34.projetAL.entity.composant.SSD;
import fr.afcepf.al34.projetAL.entity.composant.Ventirad;

@Component
@Transactional
public class ConfigurateurServiceImpl extends GenericServiceImpl<Produit> implements ConfigurateurService {

	@Autowired
	private ProduitDao produitDao;
	
	@Autowired
	private TypeProduitDao typeProduitDao;
	
	@Override
	public CrudRepository<Produit, Long> getDao() {
		return produitDao;
	}

	@Override
	public List<TypeProduit> getTypesComposants() {
		ArrayList<TypeProduit> r = new ArrayList<TypeProduit>();
		
		r.add(typeProduitDao.findByNom("Processeur"));
		r.add(typeProduitDao.findByNom("Ventirad"));
		r.add(typeProduitDao.findByNom("Carte mère"));
		r.add(typeProduitDao.findByNom("RAM"));
		r.add(typeProduitDao.findByNom("SSD"));
		r.add(typeProduitDao.findByNom("Carte graphique"));
		r.add(typeProduitDao.findByNom("Boitier"));
		r.add(typeProduitDao.findByNom("Alimentation"));
		r.add(typeProduitDao.findByNom("Carte réseau"));
		return r;
	}

	@Override
	public boolean estCompatibleAvec(Produit premier, Produit second) {
		if (premier == null || second == null)
			return true;
		
		if (premier.equals(second))
			return true;		
		
		ComposantOrdinateur premierComp = getComposantOrdinateur(premier);
		ComposantOrdinateur secondComp = getComposantOrdinateur(second);
		
		if (premierComp == null || secondComp == null)
			return true;
		
		return premierComp.estCompatibleAvec(secondComp);
	}

	private ComposantOrdinateur getComposantOrdinateur(Produit p) {
		String nomType = p.getType().getNom();
		
		switch(nomType) {
		case "Processeur":			return new Processeur(p);
		case "Ventirad":			return new Ventirad(p);
		case "Carte mère":			return new CarteMere(p);
		case "RAM":					return new RAM(p);
		case "SSD":					return new SSD(p);
		case "CarteGraphique":		return new CarteGraphique(p);
		case "Boitier":				return new Boitier(p);
		case "Alimentation":		return new Alimentation(p);
		case "CarteReseau":			return new CarteReseau(p);
		}
		
		return null;
	}

}
