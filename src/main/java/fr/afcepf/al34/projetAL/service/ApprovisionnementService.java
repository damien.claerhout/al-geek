package fr.afcepf.al34.projetAL.service;

import java.util.List;

import fr.afcepf.al34.projetAL.entity.Approvisionnement;
import fr.afcepf.al34.projetAL.entity.Fournisseur;
import fr.afcepf.al34.projetAL.entity.Stock;




public interface ApprovisionnementService extends GenericService<Approvisionnement> {
			
	public List<Approvisionnement> findByStock(Stock stock);
	public List<Approvisionnement> findByFournisseur(Fournisseur fournisseur);
}
