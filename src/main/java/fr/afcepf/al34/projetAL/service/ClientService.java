package fr.afcepf.al34.projetAL.service;

import fr.afcepf.al34.projetAL.entity.Client;

public interface ClientService extends GenericService<Client> {
	
	Client doConnecter(String email, String password);
	
	public Client findByEmail(String email);
}
