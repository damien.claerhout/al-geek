package fr.afcepf.al34.projetAL.service;

import java.util.ConcurrentModificationException;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.projetAL.entity.LigneCommande;
import fr.afcepf.al34.projetAL.entity.Panier;
import fr.afcepf.al34.projetAL.entity.Produit;


@Component
@Transactional
public class PanierServiceImpl implements PanierService {

	@Override
	public Panier ajoutPanier(Panier panier, Produit produit) {
		for (LigneCommande lc : panier.getListLC()) {
			if(lc.getProduit().getId().equals(produit.getId())) {
				lc.setQuantite(lc.getQuantite() + produit.getQuantite());
			}
		}
		if(!verifierDansPanier(panier, produit)) {
			LigneCommande newlc = new LigneCommande(null,
					produit.getQuantite(),
					produit);
			panier.getListLC().add(newlc);			
		}
		return panier;

	}
	
	private boolean verifierDansPanier(Panier panier, Produit produit) {
		boolean isPresent = false;
		for (LigneCommande lc : panier.getListLC()) {
			if (lc.getProduit().getId().equals(produit.getId())) {
				return true;
			} 
		}
		return isPresent;
	}

	
	public Panier retirerProduitDuPanier(Panier panier, Produit produit) {
		
		try {
			for (LigneCommande ligneCommande : panier.getListLC()) {
				if (ligneCommande.getProduit().equals(produit)) {

					if (ligneCommande.getQuantite() >= 1) {
						
//Decommenter ces lignes si vous voulez supprimer un article seulement au lieu de tout vider						
//						lc.setQuantite(lc.getQuantite() - 1);
//					} else {					
						panier.getListLC().remove(ligneCommande);	
					}
				}
			}
		} catch (ConcurrentModificationException e) {
			e.printStackTrace();
		}
		return panier;
		
	}	

}
