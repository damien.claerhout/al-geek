package fr.afcepf.al34.projetAL.service.utils;

import java.util.Comparator;

import fr.afcepf.al34.projetAL.entity.Produit;

public class TriMarqueCroissant implements Comparator<Produit> {

	@Override
	public int compare(Produit o1, Produit o2) {
		return o1.getMarque().getNom().compareToIgnoreCase(o2.getMarque().getNom());
	}

}
