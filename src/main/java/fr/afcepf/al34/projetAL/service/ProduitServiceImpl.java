package fr.afcepf.al34.projetAL.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.projetAL.dao.CaracteristiqueDao;
import fr.afcepf.al34.projetAL.dao.ProduitDao;
import fr.afcepf.al34.projetAL.dao.TypeProduitDao;
import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.entity.TypeProduit;
import fr.afcepf.al34.projetAL.service.utils.TriPrixCroissant;

@Component
@Transactional
public class ProduitServiceImpl extends GenericServiceImpl<Produit> implements ProduitService {

	@Autowired
	private ProduitDao dao;

	@Autowired
	private CaracteristiqueDao caracDao;

	@Autowired
	private TypeProduitDao typeProduitDao;

	@Override
	public CrudRepository<Produit, Long> getDao() {
		return dao;
	}

	public void setDao(ProduitDao dao) {
		this.dao = dao;
	}

	@Override
	public Produit getProduitAvecCaracteristiques(Long idProduit) {
		Produit produit = rechercherParId(idProduit);

		if (produit == null)
			return null;

		produit.setCaracteristiques(caracDao.findByProduit(produit));

		return produit;
	}

	@Override
	public List<Produit> getProduitsParType(Long id, boolean chargerCaracs) {
		List<Produit> result = dao.findByTypeId(id);

		if (chargerCaracs) {
			for (Produit produit : result) {
				produit.setCaracteristiques(caracDao.findByProduit(produit));
			}
		}

		return result;
	}

	@Override
	public List<TypeProduit> getTousTypesProduits() {
		return (List<TypeProduit>) typeProduitDao.findAll();
	}

	@Override
	public List<Produit> filtrerUsageOccasionnel(List<Produit> listP, String typeOrdi) {
		List<Produit> listFiltree = new ArrayList<Produit>();
		for (Produit produit : listP) {
			if (produit.getStringAttribute(typeOrdi).equals("bureautique")) {
				listFiltree.add(produit);				
			}
		}
		return listFiltree;
	}

	@Override
	public List<Produit> filtrerUsageRegulier(List<Produit> listP, String typeOrdi) {
		List<Produit> listFiltree = new ArrayList<Produit>();
		for (Produit produit : listP) {
			if (produit.getStringAttribute(typeOrdi).equals("multimedia")) {
				listFiltree.add(produit);				
			}
		}
		return listFiltree;
	}

	@Override
	public List<Produit> filtrerUsageIntensif(List<Produit> listP, String typeOrdi) {
		List<Produit> listFiltree = new ArrayList<Produit>();
		for (Produit produit : listP) {
			if (produit.getStringAttribute(typeOrdi).equals("gaming")) {
				listFiltree.add(produit);				
			}
		}
		return listFiltree;
	}

	@Override
	public List<Produit> trierMoinsCherAuPlusCher(Long idType) {
		List<Produit> listTriPrix = getProduitsParType(idType, true);
		listTriPrix.sort(new TriPrixCroissant());
				
		return listTriPrix;
	}




}
