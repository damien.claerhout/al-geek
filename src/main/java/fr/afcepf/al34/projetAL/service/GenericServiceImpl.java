package fr.afcepf.al34.projetAL.service;

import java.util.List;

import org.springframework.data.repository.CrudRepository;


/**
 * Classe abstraite de base pour la couche business.
 * 
 * Elle fournit des raccourcis vers les méthodes du DAO. Elle devrait être
 * utilisée pour les classes business qui vont travailler avec un type d'entité
 * en particulier.
 * 
 * @author pckerneis
 *
 * @param <T> l'entitée à gérer
 */
public abstract class GenericServiceImpl<T> implements GenericService<T> {

	/**
	 * Les classes dérivées doivent redéfinir cette méthode pour retourner
	 * un dao.
	 * 
	 * @return une instance spécialisée de dao
	 */
	public abstract CrudRepository<T, Long> getDao();
	
	@Override
	public T ajouter(T t) {
		return getDao().save(t);
	}

	@Override
	public boolean supprimer(T t) {
		try {
			getDao().delete(t);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public T modifier(T t) {
		return getDao().save(t);
	}

	@Override
	public T rechercherParId(Long i) {
		return getDao().findById(i).orElse(null);
	}
	
	@Override
	public List<T> getAll() {
		return (List<T>) getDao().findAll();
	}

}
