package fr.afcepf.al34.projetAL.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.projetAL.dao.LigneCommandeDao;
import fr.afcepf.al34.projetAL.entity.LigneCommande;

@Component
@Transactional
public class LigneCommandeServiceImpl extends GenericServiceImpl<LigneCommande> implements LigneCommandeService {

	@Autowired
	private LigneCommandeDao dao;
	
	@Override
	public CrudRepository<LigneCommande, Long> getDao() {
		return dao;
	}

	@Override
	public List<LigneCommande> getByCommande(Long commandeId) {
		
		return dao.getByCommande(commandeId);
	}



}
