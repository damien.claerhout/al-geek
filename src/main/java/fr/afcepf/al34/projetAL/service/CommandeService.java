package fr.afcepf.al34.projetAL.service;

import java.util.Date;
import java.util.List;

import fr.afcepf.al34.projetAL.entity.Commande;

public interface CommandeService extends GenericService<Commande> {
	
	List<Commande> getCommandesPourClient(Long idClient);
	
	List<Commande> getCommandesAtraiter();
	
	List<Commande> findByDateCreation(Date date);
	
	Commande validerCommande(Commande commande);
}
