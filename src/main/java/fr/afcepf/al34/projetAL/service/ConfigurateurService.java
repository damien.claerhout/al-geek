package fr.afcepf.al34.projetAL.service;

import java.util.List;

import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.entity.TypeProduit;

public interface ConfigurateurService extends GenericService<Produit> {
	
	List<TypeProduit> getTypesComposants();
	
	boolean estCompatibleAvec(Produit premier, Produit second);
	
}
