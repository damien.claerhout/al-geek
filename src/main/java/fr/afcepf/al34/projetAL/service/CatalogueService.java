package fr.afcepf.al34.projetAL.service;

import java.util.List;

import fr.afcepf.al34.projetAL.entity.Produit;


public interface CatalogueService extends GenericService<Produit> {
	
	List<Produit> getMeilleuresVentes(int numResultatsMax);

	List<Produit> getNouveautes();
	
}
