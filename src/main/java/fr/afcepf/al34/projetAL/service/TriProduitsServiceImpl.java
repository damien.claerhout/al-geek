package fr.afcepf.al34.projetAL.service;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.service.utils.TriMarqueCroissant;
import fr.afcepf.al34.projetAL.service.utils.TriMarqueDecroissant;
import fr.afcepf.al34.projetAL.service.utils.TriNomAlphabetique;
import fr.afcepf.al34.projetAL.service.utils.TriPrixCroissant;
import fr.afcepf.al34.projetAL.service.utils.TriPrixDecroissant;


@Component
@Transactional
public class TriProduitsServiceImpl implements TriProduitsService{
	

	@Override
	public List<Produit> trierParMarqueCroissant(List<Produit> produits) {
		produits.sort(new TriMarqueCroissant());
		return produits;		
	}

	@Override
	public List<Produit> trierParMarqueDecroissant(List<Produit> produits) {
		produits.sort(new TriMarqueDecroissant());
		return produits;
	}

	@Override
	public List<Produit> trierParPrixCroissant(List<Produit> produits) {
		produits.sort(new TriPrixCroissant());
		return produits;
	}

	@Override
	public List<Produit> trierParPrixDecroissant(List<Produit> produits) {
		produits.sort(new TriPrixDecroissant());
		return produits;
	}

	@Override
	public List<Produit> trierParNomAlphabetique(List<Produit> produits) {
		produits.sort(new TriNomAlphabetique());
		return produits;
	}

}
