package fr.afcepf.al34.projetAL.service.utils;

import java.util.Comparator;

import fr.afcepf.al34.projetAL.entity.Produit;

public class TriNomAlphabetique implements Comparator<Produit> {

	@Override
	public int compare(Produit o1, Produit o2) {
		return o1.getNom().compareTo(o2.getNom());
	}

	
}
