package fr.afcepf.al34.projetAL.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.projetAL.dao.VilleDao;
import fr.afcepf.al34.projetAL.entity.Ville;


@Component
@Transactional
public class VilleServiceImpl extends GenericServiceImpl<Ville> implements VilleService {

	@Autowired
	private VilleDao dao;
	

	
	@Override
	public CrudRepository<Ville, Long> getDao() {
		return dao;
	}


}
