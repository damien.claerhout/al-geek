package fr.afcepf.al34.projetAL.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.projetAL.dao.PaiementDiffereDao;
import fr.afcepf.al34.projetAL.entity.PaiementDiffere;


@Component
@Transactional
public class PaiementDiffereServiceImpl  extends GenericServiceImpl<PaiementDiffere> implements PaiementDiffereService {
	
	@Autowired
	private PaiementDiffereDao dao;
	
	@Override
	public CrudRepository<PaiementDiffere, Long> getDao() {
		return dao;
	}

}
