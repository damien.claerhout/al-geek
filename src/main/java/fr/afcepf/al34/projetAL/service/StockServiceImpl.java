package fr.afcepf.al34.projetAL.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.projetAL.dao.StockDao;
import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.entity.Stock;



@Component
@Transactional
public class StockServiceImpl extends GenericServiceImpl<Stock> implements StockService {

	@Autowired
	private StockDao dao;
	
	
	
	@Override
	public CrudRepository<Stock, Long> getDao() {
		return dao;
	}



	@Override
	public List<Stock> getByTypeProduit(Long typeProduitId) {
		
		return dao.getByTypeProduit(typeProduitId);

	}



	@Override
	public Stock getByProduit(Produit produit) {
		
		return dao.findByProduit(produit);
	}
	
	@Override
	public List<Stock> getByQuantiteDispo(int quantite){
		
		return dao.getByQuantiteDispo(quantite);
	}
	
	


}
