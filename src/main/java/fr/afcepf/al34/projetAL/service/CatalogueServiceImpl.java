package fr.afcepf.al34.projetAL.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.projetAL.dao.CommandeDao;
import fr.afcepf.al34.projetAL.dao.ProduitDao;
import fr.afcepf.al34.projetAL.entity.Commande;
import fr.afcepf.al34.projetAL.entity.LigneCommande;
import fr.afcepf.al34.projetAL.entity.Produit;

@Component
@Transactional
public class CatalogueServiceImpl extends GenericServiceImpl<Produit> implements CatalogueService {

	@Autowired
	private ProduitDao dao;
	
	@Autowired
	private CommandeDao cmdDao;

	@Override
	public CrudRepository<Produit, Long> getDao() {
		return dao;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Produit> getMeilleuresVentes(int numResultatsMax) {
		Map<Produit, Integer> numVentes = new HashMap<>();
		
		List<Commande> commandes = (List<Commande>) cmdDao.findAll();
		
		for (Commande cmd : commandes) {
			for (LigneCommande ligne : cmd.getListLigneCommande()) {
				Produit p = ligne.getProduit();
				int num = ligne.getQuantite();
				
				if (numVentes.containsKey(p)) {
					numVentes.put(p, numVentes.get(p) + num);
				} else {
					numVentes.put(p, num);
				}
			}
		}
		
		Stream<Entry<Produit, Integer>> stream = numVentes.entrySet()
		  .stream()
		  .sorted(Map.Entry.comparingByValue());
		
		List<Produit> result = new ArrayList<Produit>();
		int numResultats = 0;
		
		for (Object entry : stream.toArray()) {
			result.add(((Entry<Produit, Integer>) entry).getKey());
			
			if (++ numResultats >= numResultatsMax)
				break;
		}

		return result;
	}

	@Override
	public List<Produit> getNouveautes() {
		return dao.findTop30ByOrderByDateAjoutDesc();
	}

}