package fr.afcepf.al34.projetAL.service;

import java.util.List;

public interface GenericService<T> {
    T ajouter(T t);
    boolean supprimer (T t);
    T modifier(T t);
    T rechercherParId (Long i);
    List<T> getAll();
}
