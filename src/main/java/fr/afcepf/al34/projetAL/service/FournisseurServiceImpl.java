package fr.afcepf.al34.projetAL.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.projetAL.dao.FournisseurDao;
import fr.afcepf.al34.projetAL.entity.Fournisseur;


@Component
@Transactional
public class FournisseurServiceImpl extends GenericServiceImpl<Fournisseur> implements FournisseurService {

	@Autowired
	private FournisseurDao dao;
	

	
	@Override
	public CrudRepository<Fournisseur, Long> getDao() {
		return dao;
	}


}
