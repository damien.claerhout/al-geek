package fr.afcepf.al34.projetAL.service;

import fr.afcepf.al34.projetAL.entity.InfosBancaires;

public interface InfosBancairesService extends GenericService<InfosBancaires> {

}
