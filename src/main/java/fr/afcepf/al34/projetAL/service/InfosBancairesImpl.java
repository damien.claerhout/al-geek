package fr.afcepf.al34.projetAL.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import fr.afcepf.al34.projetAL.dao.InfosBancairesDao;
import fr.afcepf.al34.projetAL.entity.InfosBancaires;

@Component
public class InfosBancairesImpl extends GenericServiceImpl<InfosBancaires> implements InfosBancairesService{

	@Autowired
	private InfosBancairesDao dao;
	
	@Override
	public CrudRepository<InfosBancaires, Long> getDao() {
		return dao;
	}

}
