package fr.afcepf.al34.projetAL.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.annotation.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import fr.afcepf.al34.projetAL.entity.Marque;
import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.service.CatalogueService;
import fr.afcepf.al34.projetAL.service.ProduitService;
import fr.afcepf.al34.projetAL.service.TriProduitsService;
import fr.afcepf.al34.projetAL.service.utils.TriMarqueCroissant;
import fr.afcepf.al34.projetAL.service.utils.TriMarqueDecroissant;
import fr.afcepf.al34.projetAL.service.utils.TriNomAlphabetique;
import fr.afcepf.al34.projetAL.service.utils.TriPrixCroissant;
import fr.afcepf.al34.projetAL.service.utils.TriPrixDecroissant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ManagedBean
@ViewScoped
@Getter @Setter @NoArgsConstructor
public class CatalogueBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<Produit> produitsFiltres;
	
	private List<Produit> produits;
	
	private List<Marque> filtreMarques;
	
	private List<Marque> marquesVisibles;
	
	private double filtrePrixMin;
	
	private double filtrePrixMax;
	
	private double prixMinVisible;
	
	private double prixMaxVisible;
	
	private String filtreReference;
	
	@ManagedProperty("#{param.triChoisi}")
	private String triChoisi;
	
	@ManagedProperty("#{param.typeProduitId}")
	private Long typeProduitId;
		
	@Autowired
	private CatalogueService catalogueService;
		
	@Autowired
	private ProduitService produitService;
	
	@Autowired
	private TriProduitsService triProduitService;
	
	
	@PostConstruct
	public void init() {
		String paramId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("typeProduitId");
		String paramTri = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("triChoisi");
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		
		if (paramId != null) {
			session.setAttribute("paramId", paramId);
		}
		
		if (paramTri != null) {
			session.setAttribute("paramTri", paramTri);
		}
		
		if (paramId == null) {
			paramId = (String) session.getAttribute("paramId");
		}
		
		if (paramTri == null) {
			paramTri =(String) session.getAttribute("paramTri");
			
		}

		
		try {
			if (paramId != null) {
				typeProduitId = Long.parseLong(paramId);
			}

			
			triChoisi = paramTri;

		} catch (Exception e) {
			e.printStackTrace();
			
			typeProduitId = null;
		}
		
		filtreMarques = new ArrayList<Marque>();
		marquesVisibles = new ArrayList<Marque>();
		
		chargerCatalogue();
				
		List<Produit> meilleuresVentes = catalogueService.getMeilleuresVentes(5);
				
	}
	
	@PostConstruct
	public void refreshCatalogue() {
		catalogueService.getAll();
	}
	
	public void chargerCatalogue() {
		produits = produitService.getProduitsParType(typeProduitId, false);
		
		marquesVisibles = new ArrayList<Marque>();
		
		// Préparation des filtres
		double minPrix = -1;
		double maxPrix = -1;

		for (Produit p : produits) {
			Marque m = p.getMarque();

			
			// Ajout des marques
			if (m != null && !marquesVisibles.contains(m)) {
				marquesVisibles.add(p.getMarque());
			}
			
			// Recherche des prix min et max
			if (minPrix < 0 || minPrix > p.getPrix())
				minPrix = p.getPrix();

			if (maxPrix < 0 || maxPrix < p.getPrix())
				maxPrix = p.getPrix();
		}
		
		// Limites du slider de prix
		prixMinVisible = minPrix > 0 ? minPrix : 0;
		prixMaxVisible = maxPrix > 0 ? maxPrix : 0;
		
		// Selection du slider de prix (par défaut, aux limites du slider)
		filtrePrixMax = prixMaxVisible;
		filtrePrixMin = prixMinVisible;
		
		appliquerFiltres();
	}

	public String afficher(Long id) {
		typeProduitId = id;
		return "catalogue.xhtml?faces-redirect=true&includeViewParams=true";
	}

	public void appliquerFiltres() {
		produitsFiltres = new ArrayList<Produit>();
		
		boolean filtrerParMarques = ! filtreMarques.isEmpty();
		
		for (Produit p : produits) {
			if (p.getPrix() < prixMinVisible || p.getPrix() > prixMaxVisible)
				continue;
			
			if (filtrerParMarques && ! filtreMarques.contains(p.getMarque()))
				continue;
			
			if (filtreReference != null && ! filtreReference.isEmpty())
				if (! p.getNom().toLowerCase().contains(filtreReference.toLowerCase()))
					continue;
			
			produitsFiltres.add(p);
			trierProduits();
		}
		
	}
	
	public void trierProduits() {
		if (null != triChoisi && !triChoisi.isEmpty()) {
			
			if (triChoisi.contentEquals("triNomAlpha")) produitsFiltres.sort(new TriNomAlphabetique());
			if (triChoisi.contentEquals("triPrixCroissant")) produitsFiltres.sort(new TriPrixCroissant());
			if (triChoisi.contentEquals("triPrixDecroissant")) produitsFiltres.sort(new TriPrixDecroissant());
			if (triChoisi.contentEquals("triMarqueCroissant")) produitsFiltres.sort(new TriMarqueCroissant());
			if (triChoisi.contentEquals("triMarqueDecroissant")) produitsFiltres.sort(new TriMarqueDecroissant());
			
//			if (triChoisi.contentEquals("triNomAlpha")) {
//				produitsFiltres = triProduitDelegate.trierParNomAlphabetique(produitsFiltres);
//			}
//			
//			if (triChoisi.contentEquals("triPrixCroissant")) {
//				produitsFiltres = triProduitDelegate.trierParPrixCroissant(produitsFiltres);
//			}
//			
//			if (triChoisi.contentEquals("triPrixDecroissant")) {
//				produitsFiltres = triProduitDelegate.trierParPrixDecroissant(produitsFiltres);
//			}
//			
//			if (triChoisi.contentEquals("triMarqueCroissant")) {
//				produitsFiltres = triProduitDelegate.trierParMarqueCroissant(produitsFiltres);
//			}
//			
//			if (triChoisi.contentEquals("triMarqueDecroissant")) {
//				produitsFiltres = triProduitDelegate.trierParMarqueDecroissant(produitsFiltres);
//			}
//			
		} else {
			
			produitsFiltres.sort(new TriNomAlphabetique());
			
			//produitsFiltres = triProduitDelegate.trierParNomAlphabetique(produitsFiltres);
			
		}

	}
}
