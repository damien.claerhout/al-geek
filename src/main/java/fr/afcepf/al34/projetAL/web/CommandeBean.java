package fr.afcepf.al34.projetAL.web;

import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import fr.afcepf.al34.projetAL.delegate.CommandeDelegate;
import fr.afcepf.al34.projetAL.delegate.InfosBancairesDelegate;
import fr.afcepf.al34.projetAL.delegate.PaiementDiffereDelegate;
import fr.afcepf.al34.projetAL.entity.Commande;
import fr.afcepf.al34.projetAL.entity.InfosBancaires;
import fr.afcepf.al34.projetAL.entity.LigneCommande;
import fr.afcepf.al34.projetAL.entity.PaiementDiffere;
import fr.afcepf.al34.projetAL.entity.Panier;
import fr.afcepf.al34.projetAL.service.CommandeService;
import fr.afcepf.al34.projetAL.service.InfosBancairesService;
import fr.afcepf.al34.projetAL.service.PaiementDiffereService;
import lombok.Getter;
import lombok.Setter;

@ManagedBean
@SessionScoped
public class CommandeBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
//	@Autowired
//	private CommandeDelegate commandeDelegate;
//		
//	@Autowired
//	private InfosBancairesDelegate infosBankDelegate;
//	
	@Autowired
	private PaiementDiffereDelegate paiementDiffereDelegate;
	
	@Autowired
	private CommandeService commandeService;
		
	@Autowired
	private InfosBancairesService infosBankService;
	
	@Autowired
	private PaiementDiffereService paiementDiffereService;
	
	@Inject
	private PanierBean panierBean;
	

	private PaiementDiffere paiementDiffere;
	
	@Inject
	private ConnectBean connectBean;
	
	@Getter @Setter
	private Commande commande;
	
	@Getter @Setter
	private InfosBancaires infosBank= new InfosBancaires();
	
	@Getter @Setter
	private boolean servicePaiementDiffereIsAvalaible = true;
	
	// test
	@Getter @Setter
	private String login;
	
	private int selectedNbreMois;
	
	private double mensualite;
	
	
	
	@PostConstruct
	public void init() {
		

		checkIfServicePaiementDifIsAvailable();


		paiementDiffere=new PaiementDiffere();
	}
	
		
	public String effectuerCommande() {	
		if (connectBean.checkClientConnecte()) {
			return "pageConnexion.xhtml?faces-redirect=true";
		} else {
			commande = new Commande();
			commande.setListLigneCommande(new ArrayList<LigneCommande>());
			commande.setListLigneCommande(panierBean.getPanier().getListLC());
			return "macommande.xhtml?faces-redirect=true";
		}
	}
	
	
	public String validerPaiement() {	
		
		
		// persistance des coord.bancaires ( !!! pas bien ...)
		infosBank = infosBankService.ajouter(infosBank);

		// persistance de la Commande
		double prixTotalCommande = panierBean.afficherPrixTotal();
		
		commande.setClient(connectBean.getClient());
		commande.setDateCreation(new Date());
		commande.setPrix(prixTotalCommande);		
		commande.setInfosBank(infosBank);
		

		commande=commandeService.validerCommande(commande);
		
		// persistance du PaiementDiffere
		if(selectedNbreMois>1) {
			paiementDiffere.setCommande(commande);
			paiementDiffere.setMensualité(mensualite);
			paiementDiffere.setClient(commande.getClient());
			paiementDiffere.setSomme(commande.getPrix());
			paiementDiffere.setMois(selectedNbreMois);
			paiementDiffere = paiementDiffereService.ajouter(paiementDiffere);
		}
		
		// reinitialisation du panier et du paiement différé
		panierBean.setPanier(new Panier());
		panierBean.getPanier().setListLC(new ArrayList<LigneCommande>());
		infosBank = new InfosBancaires();
		paiementDiffere = new PaiementDiffere();
		selectedNbreMois=1;
		return "commandeeffectuee.xhtml?faces-redirect=true";
	}
	
	public void checkIfServicePaiementDifIsAvailable() {
		
		int code = 200;
		try {
			// URL qui pointe sur le service déployé sur AWS
			URL siteURL = new URL("http://35.180.125.66:8080/algeekdif/paiement?a=100&b=2");
			//URL siteURL = new URL("http://localhost:8090/paiement?a=100&b=2");
			HttpURLConnection connection = (HttpURLConnection) siteURL.openConnection();
			connection.setRequestMethod("GET");
			connection.setConnectTimeout(200);
			connection.connect();
 
			code = connection.getResponseCode();
			if (code == 200) {
				servicePaiementDiffereIsAvalaible = true;
				
			} else {
				servicePaiementDiffereIsAvalaible = false;
			}
		} catch (Exception e) {
			servicePaiementDiffereIsAvalaible = false;
 
		}
	}
	
	
	public void boutonDemoCodeBank() {
		infosBank.setTypeCarte("Visa");
		infosBank.setNumCarte("4970597258462195");
		infosBank.setDateCarte(new Date());
		infosBank.setCryptogramme("842");
//		return "macommande.xhtml?faces-redirect=true";
		
	}
	
	public void calculMensualite() {

		mensualite=paiementDiffereDelegate.findPaiementByMensualite(panierBean.afficherPrixTotal(),selectedNbreMois);
	}


	public PaiementDiffere getPaiementDiffere() {
		return paiementDiffere;
	}


	public void setPaiementDiffere(PaiementDiffere paiementDiffere) {
		this.paiementDiffere = paiementDiffere;
	}


	public int getSelectedNbreMois() {
		return selectedNbreMois;
	}


	public void setSelectedNbreMois(int selectedNbreMois) {
		this.selectedNbreMois = selectedNbreMois;
	}


	public double getMensualite() {
		return mensualite;
	}


	public void setMensualite(int mensualite) {
		this.mensualite = mensualite;
	}


}
