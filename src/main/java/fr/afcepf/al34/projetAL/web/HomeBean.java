package fr.afcepf.al34.projetAL.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;

import fr.afcepf.al34.projetAL.delegate.CatalogueDelegate;
import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.service.CatalogueService;
import fr.afcepf.al34.projetAL.service.ProduitService;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ManagedBean
@ViewScoped
@Getter @Setter @NoArgsConstructor
public class HomeBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ProduitService produitService;
	
	private List<Produit> meilleuresVentes;
	
	private List<Produit> nouveautes;
	
	@Autowired
	private CatalogueService catalogueService;
	
	@PostConstruct
	public void init() {

		
		nouveautes = catalogueService.getNouveautes();
		meilleuresVentes = catalogueService.getMeilleuresVentes(10);
	}
}
