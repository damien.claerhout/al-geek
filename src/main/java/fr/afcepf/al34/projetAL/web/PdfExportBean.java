package fr.afcepf.al34.projetAL.web;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.annotation.SessionScope;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

import fr.afcepf.al34.projetAL.delegate.CommandeDelegate;
import fr.afcepf.al34.projetAL.entity.Commande;
import fr.afcepf.al34.projetAL.entity.LigneCommande;
import lombok.Getter;
import lombok.Setter;


@Controller
@Named
@SessionScope
public class PdfExportBean implements Serializable { 

	private static final long serialVersionUID = 7609134248482865644L;

	@Getter @Setter
	boolean pdfGenerate = false;
	
	@Autowired
	private CommandeDelegate commandeDelegate;
	
	@Inject
	private CommandeBean commandeBean;
	
	
	@RequestMapping(value="/pdfexport")
	protected void doGet( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {

		Long idCommand = 0L;
		try { 
			idCommand = Long.parseLong( request.getParameter( "id" ) );
		} catch (Exception exception ) {
			// Nothing to do
		}
		
		final ServletContext servletContext = request.getSession().getServletContext();
		final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
		final String temporaryFilePath = tempDirectory.getAbsolutePath();
		//			String masterPath = request.getServletContext().getRealPath( "/META-INF/resources/JS/modeleFacture.pdf" );
		String fileName = "Facture_AL_Geek_n°_" + idCommand+ ".pdf";
		response.setContentType( "application/pdf" );
		response.setHeader("Content-disposition", "attachment; filename="+ fileName);


		createPDF(temporaryFilePath+"\\"+fileName, idCommand);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		baos = convertPDF(temporaryFilePath+"\\"+fileName);
		
		OutputStream os = response.getOutputStream();
		baos.writeTo(os);
		os.flush();
		
		idCommand = new Long(0L);

	}


	private Document createPDF(String file, Long id)  {
		Commande commandeTest = commandeDelegate.rechercherParId(id);
		
		Font policeIdentite = new Font(Font.HELVETICA, 16, Font.NORMAL);
		Font policeColonne = new Font (Font.HELVETICA, 16, Font.BOLD);
		Font policeElement = new Font (Font.HELVETICA, 16, Font.NORMAL);
		Font policeMontantTotal = new Font (Font.HELVETICA, 18, Font.BOLD);
		
		NumberFormat formatter = NumberFormat.getNumberInstance();
		formatter.setMinimumFractionDigits(2);
		formatter.setMaximumFractionDigits(2);
		
		DateFormat dateformat = DateFormat.getDateTimeInstance(
		        DateFormat.SHORT,
		        DateFormat.SHORT, new Locale("FR","fr"));
		        
		Document doc = null;
		try {
			doc = new Document();
			com.lowagie.text.pdf.PdfWriter.getInstance(doc, new FileOutputStream(file));
			doc.open();
			
			PdfPTable tablogo = new PdfPTable(1);
			PdfPCell celllogo = null;			
			
			Image logo;
			try {
				logo = Image.getInstance(PdfExportBean.class.getResource("/META-INF/IMG/logoalgeek.png"));
				celllogo = new PdfPCell(logo);
				celllogo.setHorizontalAlignment(Element.ALIGN_LEFT);
				celllogo.setBorderWidth(0);
				celllogo.setBorderColor(Color.getColor("RED"));
				tablogo.addCell(celllogo);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			doc.add(tablogo);
			
			Paragraph p = new Paragraph(commandeTest.getClient().getPrenom() + " " + commandeTest.getClient().getNom(),policeIdentite);
			p.setIndentationLeft(280f);
			p.setSpacingBefore(10f);
			Paragraph p2 = new Paragraph(commandeTest.getClient().getAdresse(),policeIdentite);
			p2.setIndentationLeft(280f);
			Paragraph p3 = new Paragraph(commandeTest.getClient().getCodePostal() + " " + commandeTest.getClient().getVille(),policeIdentite);
			p3.setIndentationLeft(280f);
			doc.add(p);
			doc.add(p2);
			doc.add(p3);
			
			Paragraph p4 = new Paragraph("Facture n° " + commandeTest.getId());
			p4.setSpacingBefore(90f);
			doc.add(p4);
			Paragraph p5 = new Paragraph("Commande réalisée le " + dateformat.format(commandeTest.getDateCreation()));
			doc.add(p5);
			
			
			PdfPTable tab = new PdfPTable(3);
			tab.setSpacingBefore(20f);
			float[] larg = {7f,73f,20f};
			tab.setWidths(larg);
			tab.setWidthPercentage(100);
			
			PdfPCell cell;
			cell = new PdfPCell(new Phrase(("\rQté\n\r"), policeColonne));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setRowspan(1);
			tab.addCell(cell);
			
			cell = new PdfPCell(new Phrase(("\rProduit\n\r"), policeColonne));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setRowspan(1);
			tab.addCell(cell);
			
			cell = new PdfPCell(new Phrase(("\rPrix\n\r"), policeColonne));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setRowspan(1);
			tab.addCell(cell);
			
			for (LigneCommande lc : commandeTest.getListLigneCommande()) {
				
				cell = new PdfPCell(new Phrase((lc.getQuantite().toString()),policeElement));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_CENTER);
				tab.addCell(cell);
				
				cell = new PdfPCell(new Phrase((lc.getProduit().getNom()),policeElement));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_CENTER);
				tab.addCell(cell);
				
				cell = new PdfPCell(new Phrase((formatter.format(lc.getPrixTotal()) + " €"),policeElement));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_CENTER);
				tab.addCell(cell);
						
			}

			doc.add(tab);

			Paragraph p9 = new Paragraph("Montant total : " + formatter.format(commandeTest.getPrix()) +  " €", policeMontantTotal);
			p9.setIndentationLeft(280f);
			p9.setSpacingBefore(100f);
			doc.add(p9);
			
			// paiement différé
			
			doc.close();
			} catch (FileNotFoundException e) {

			} catch (DocumentException e)  {

			}
		return doc;
	}
	
	
	private ByteArrayOutputStream convertPDF(String fileName) {
		InputStream inputStream  = null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			inputStream = new FileInputStream(fileName);
			byte[] buffer = new byte[1024];
			baos = new ByteArrayOutputStream();
			int bytesRead;
			while((bytesRead = inputStream.read(buffer)) != -1) {
				baos.write(buffer, 0, bytesRead);
			}
		} catch (FileNotFoundException e) {

		} catch (IOException e ){ 

		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {

				}
			}
		}

		return baos;
	}
	
	public boolean okpdf() {
		this.pdfGenerate = true;
		return pdfGenerate;
	}


	public CommandeBean getCommandeBean() {
		return commandeBean;
	}


	public void setCommandeBean(CommandeBean commandeBean) {
		this.commandeBean = commandeBean;
	}
	
	

}