package fr.afcepf.al34.projetAL.web;

import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuElement;
import org.primefaces.model.menu.MenuModel;
import org.springframework.beans.factory.annotation.Autowired;

import fr.afcepf.al34.projetAL.delegate.CategorieDelegate;
import fr.afcepf.al34.projetAL.entity.Categorie;
import fr.afcepf.al34.projetAL.service.CategorieService;

@ManagedBean
@ApplicationScoped
public class MenuCategoriesBean implements Serializable {

	private static final long serialVersionUID = 3325278823042584862L;

	private MenuModel model;

	@Autowired
	private CategorieService categorieService;
		

	private boolean serviceDepanneurIsAvalaible = true;
	
	

	@PostConstruct
	public void init() {
	
		checkIfServiceAvalaible();
	
		model = new DefaultMenuModel();
			
		Categorie root = categorieService.getRootCategorie();
		
		MenuElement submenu = construireRecursif(root);
		model.getElements().add(submenu);
		
		DefaultMenuItem configItem = new DefaultMenuItem("Montage sur mesure", "", "configurateur.xhtml");
		model.getElements().add(configItem);
		
		DefaultMenuItem achatGuideItem = new DefaultMenuItem("Achat guidé", "", "achatguide.xhtml");
		model.getElements().add(achatGuideItem);
		
		if (serviceDepanneurIsAvalaible == true) {
			DefaultMenuItem depanneurItem = new DefaultMenuItem("Trouver un depanneur", "", "trouverundepanneur.xhtml");
			model.getElements().add(depanneurItem);
		}
	}

	private MenuElement construireRecursif(Categorie categorie) {
		if (categorie.getEnfants().size() == 0) {
			DefaultMenuItem item = new DefaultMenuItem(categorie.getNomAffiche());
			
			if (categorie.getTypeProduit() != null) {
				item.setParam("typeProduitId", categorie.getTypeProduit().getId());
				item.setCommand("#{catalogueBean.afficher(" + categorie.getTypeProduit().getId() + ")}");
			}
			
			return item;			
		}
		
		DefaultSubMenu sub = new DefaultSubMenu(categorie.getNomAffiche());
		
		for (Categorie c : categorie.getEnfants()) {
			sub.getElements().add(construireRecursif(c));			
		}
		
		return sub;
	}
	
	public void checkIfServiceAvalaible() {
		 

		int code = 200;
		try {
			// URL qui pointe sur le service déployé sur AWS
			URL siteURL = new URL("http://35.181.48.243:8080/algeekrep/");
			// URL qui pointe sur le service déployé localement
			//URL siteURL = new URL("http://192.168.33.13:8180/depanneur-api/");
			HttpURLConnection connection = (HttpURLConnection) siteURL.openConnection();
			connection.setRequestMethod("GET");
			connection.setConnectTimeout(200);
			connection.connect();
 
			code = connection.getResponseCode();
			if (code == 200) {
				serviceDepanneurIsAvalaible = true;
				
			} else {
				serviceDepanneurIsAvalaible = false;
			}
		} catch (Exception e) {
			serviceDepanneurIsAvalaible = false;
 
		}
		
	
	}

	// Getters/Setters

	public MenuModel getModel() {
		return model;
	}

	public void setModel(MenuModel model) {
		this.model = model;
	}


}
