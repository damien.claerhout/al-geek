package fr.afcepf.al34.projetAL.web;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;

import fr.afcepf.al34.projetAL.delegate.PanierDelegate;
import fr.afcepf.al34.projetAL.entity.LigneCommande;
import fr.afcepf.al34.projetAL.entity.Panier;
import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.entity.ProduitPanier;
import fr.afcepf.al34.projetAL.service.PanierService;
import lombok.Getter;
import lombok.Setter;

@ManagedBean
@SessionScoped
public class PanierBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Getter @Setter
	private Panier panier;	
	
	@Getter @Setter
	private Long idProdSelected;
	
	private boolean badgeVisible;
		
	@Inject @Getter
	private FicheProduitBean ficheProduitBean;
	
	
	@Getter @Setter
	private String titrePanier = "Panier vide";
	
	
	@Getter @Setter
	private Produit ordiSelected; // ordi selected depuis AchatGuide

	
	@Getter @Setter
	private boolean typeOrdi;  // true = ordi bureau  -  false = ordi portable
	
	@Autowired
	PanierService panierService;
		
	
	@PostConstruct
	public void init() {
		panier = new Panier();
		panier.setListLC(new ArrayList<LigneCommande>());
	}
	
	
	public String afficherPanier() {
		return "votrepanier.xhtml?faces-redirect=true";
	}
	
	public void ajoutPanier(Produit produit) {
		titrePanier = "Votre panier";
		ProduitPanier produitPanier = new ProduitPanier(panier, produit);

		panier = panierService.ajoutPanier(panier, produit);
	}
	
	
	public String ajouterAuPanierDepuisCatalogue(int quantite, Produit produit) {
		produit.setQuantite(quantite);
		ajoutPanier(produit);
		return "catalogue.xhtml?faces-redirect=true&includeViewParams=true";
	}
	
	
	
	public String ajouterAuPanierDepuisAchatGuide() {
		ajoutPanier(ordiSelected);
		return "votrepanier.xhtml?faces-redirect=true";
	}
	
	
	
	
	public String selectionnezOrdiAchatGuide(Produit p) {
		typeOrdi = typeOrdiSelected(p);
		ordiSelected = p;
		return "achatguideconfirm.xhtml?faces-redirect=true";
	}
	
	
	
	public String ajouterAuPanierDepuisFiche() {
		Produit prodSelected = ficheProduitBean.getProduit(); // null
		ajoutPanier(prodSelected);
		return "ficheproduit.xhtml";
		
	}
	
	public String retirerProduitDuPanier(Produit produit) {
		
		ProduitPanier produitPanier = new ProduitPanier(panier, produit);

		panier = panierService.retirerProduitDuPanier(panier, produit);
		
		if (panier.getListLC().size() == 0) {
			this.titrePanier = "Panier vide";			
		}
		return "votrepanier.xhtml?faces-redirect=true";
	}
	
	public void viderPanier() {
		panier = new Panier();
		panier.setListLC(new ArrayList<LigneCommande>());
	}
	
	public int afficherQuantiteProduitsDansPanier() {
		int quantite = 0;
		for (LigneCommande ligneCommande : panier.getListLC()) {
			quantite += ligneCommande.getQuantite();			
		}
		return quantite;
	}
	
	public String badgeQuantiteProduitsDansPanier() {
		Integer quantite = afficherQuantiteProduitsDansPanier();
		return quantite.toString();
	}
	
	public double afficherPrixTotal() {
		double somme = 0;
		for (LigneCommande ligneCommande : panier.getListLC()) {
			somme += ligneCommande.getProduit().getPrix() * ligneCommande.getQuantite();			
		}
		
		return somme;
	}

	
	// methode utile pour AchatGuide...
	public boolean typeOrdiSelected(Produit produit) {
		
		if (produit.getType().getId() == 8L) {
			return true;
		} else {
			return false;
		}
	}


	public boolean isBadgeVisible() {
		return badgeVisible;
	}


	public void setBadgeVisible(boolean badgeVisible) {
		this.badgeVisible = badgeVisible;
	}
	
	
	
}
