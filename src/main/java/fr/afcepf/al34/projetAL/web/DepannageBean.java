package fr.afcepf.al34.projetAL.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.annotation.SessionScope;

import fr.afcepf.al34.projetAL.delegate.DepannageDelegate;
import fr.afcepf.al34.projetAL.dto.DepanneurDto;
import lombok.Getter;
import lombok.Setter;

@ManagedBean
@SessionScope
public class DepannageBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter @Setter
	private String ville;
	
	@Getter @Setter
	private List<DepanneurDto> depanneurs;
	
	@Getter @Setter
	private String listJson;
	
	@Autowired
	private DepannageDelegate depannageDelegate;
	

	
	public String findDepanneursByVille() {
		this.depanneurs=depannageDelegate.findDepanneursByVille(this.ville);
		getListDepanneursEnJson(this.ville);
		// Pour obtenir la 1ere lettre de la ville en Majuscule
		ville = this.ville.replace(ville.charAt(0), ville.toUpperCase().charAt(0));	
		return "nosdepanneurs.xhtml?faces-redirect=true";
	}
	
	public void getListDepanneursEnJson(String ville) {
		listJson = depannageDelegate.listDepanneursEnJson(ville);
		
	}
	
	
	

}
