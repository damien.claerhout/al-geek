package fr.afcepf.al34.projetAL.web;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;

import fr.afcepf.al34.projetAL.delegate.ClientDelegate;
import fr.afcepf.al34.projetAL.entity.Client;
import fr.afcepf.al34.projetAL.entity.LoginRequest;
import lombok.Getter;
import lombok.Setter;

@Named
@SessionScoped
public class ConnectBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean overlayVisible;
	private boolean clientVide;
	
	private Client client = new Client();
	private String messageErreur;
	
	@Inject
	private PanierBean panierBean;
	
	public String getMessageErreur() {
		return messageErreur;
	}

	public void setMessageErreur(String messageErreur) {
		this.messageErreur = messageErreur;
	}


	@Getter @Setter
	private String email;
	
	@Getter @Setter
	private String password;
	
	
	@Autowired
	ClientDelegate clientDelegate;
	
	public String doConnecter() {
		LoginRequest loginRequest = new LoginRequest(email, password);
		client = clientDelegate.doConnecter(loginRequest);
		if(client==null) {
			return "/home.xhtml?faces-redirect=true";
		}
		hideOverlay();
		email = "";
		password= "";
		return  "/home.xhtml?faces-redirect=true";
	}
		
	public String doDeconnecter() {
		this.client = new Client();
		this.panierBean.viderPanier();
		return "/home.xhtml?faces-redirect=true";
	}
	
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String showOverlay() {
		overlayVisible = true;
		return null;
	}
	
	public String hideOverlay() {
		overlayVisible = false;
		return null;
	}

	public boolean isOverlayVisible() {
		return overlayVisible;
	}

	public void setOverlayVisible(boolean overlayVisible) {
		this.overlayVisible = overlayVisible;
	}

	public boolean checkClientConnecte() {
		if(null == client || null == client.getId()) {
			clientVide = true;
		}
		else {
			clientVide = false;
		}
		return clientVide;
	}
	
	public String goToEspacePerso() {
		return "/espaceperso.xhtml?faces-redirect=true";
	}
	
	
	public String goToFormulaireInscription() {
		hideOverlay();
		return "/formulaireinscription.xhtml?faces-redirect=true";
		
	}
	
	
	public void boutonDemoConnect() {
		email = "adupont@gmail.com";
		//password = "azerty";
	}
}
