package fr.afcepf.al34.projetAL.web;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;


import fr.afcepf.al34.projetAL.delegate.AuthentificationDelegate;
import fr.afcepf.al34.projetAL.delegate.ClientDelegate;
import fr.afcepf.al34.projetAL.entity.Client;
import fr.afcepf.al34.projetAL.entity.Credentials;
import fr.afcepf.al34.projetAL.entity.CredentialsInit;
import fr.afcepf.al34.projetAL.service.authentification.exception.AuthentificationException;



@Named
@SessionScoped
public class InscriptionBean implements Serializable {

	private static final long serialVersionUID = 1L;
		
	@Autowired
	private ClientDelegate clientDelegate;
	
	private String nom;
	private String prenom;
	private String email;
	private String adresse;
	private String codePostal;
	private String ville;
	private String password;
	
	@Autowired
	ConnectBean connectBean;
	
	@Autowired
	AuthentificationDelegate authentificationDelegate;
	
	public String doInscription() {
		// Client
		Client  cl= new Client();
		cl.setNom(nom);
		cl.setPrenom(prenom);
		cl.setEmail(email);
		cl.setAdresse(adresse);
		cl.setCodePostal(codePostal);
		cl.setVille(ville);
		
		
		// Credentials
		Credentials cred = new Credentials();
		CredentialsInit credInit = new CredentialsInit(cred, password);
		
		try {
			
			credInit=authentificationDelegate.initializeCredentials(credInit);
			


			cl.setSalt(credInit.getCredentials().getSalt());
			cl.setHashedPassword(credInit.getCredentials().getHashedPassword());
			

			if (clientDelegate.findByEmail(cl.getEmail()) == null) {
				cl=clientDelegate.ajouter(cl);
			}
			else {
				System.out.println("CETTE EMAIL EXISTE DEJA");
			}
		} catch (AuthentificationException e) {
			// Gérer erreur d'authentification (message erreur, log...)
			e.printStackTrace();
			return null;
		}
		
		connectBean.setClient(cl);
		return "home.xhtml?faces-redirect=true";
		
	}
	
	
	public String boutonDemoNewClient() {
		 nom = "Durand";
		 prenom = "Jean";
		 adresse = "125 avenue Clémenceau";
		 codePostal = "33000";
		 ville = "Bordeaux";
		 email = "jeandurand@yahoo.fr";
		 password = "toto123";
		return "formulaireinscription.xhtml?faces-redirect=true";
		
	}
	
	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getAdresse() {
		return adresse;
	}


	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}


	public String getCodePostal() {
		return codePostal;
	}


	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}


	public String getVille() {
		return ville;
	}


	public void setVille(String ville) {
		this.ville = ville;
	}


	
	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}

}
