package fr.afcepf.al34.projetAL.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;

import fr.afcepf.al34.projetAL.delegate.ProduitDelegate;
import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.service.ProduitService;
import lombok.Getter;
import lombok.Setter;

@ManagedBean
@SessionScoped
public class AchatGuideBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter @Setter
	private List<Produit> listPortables;
	
	@Getter @Setter
	private List<Produit> listOrdiBureau;
	
	@Getter @Setter
	private List<Produit> listFiltree;
	
	@Getter @Setter
	private List<Produit> listFiltreeParPrix;
	
	@Getter @Setter
	private boolean overlayVisible;

	@Getter @Setter
	private List<Produit> overlayProduits;

	@Getter @Setter
	private boolean ecranIsSelected = false;
	
	@Getter @Setter
	private boolean clavierIsSelected = false;
	
	@Getter @Setter
	private boolean sourisIsSelected = false;
	
	
//	@Autowired
//	private ProduitDelegate produitDelegate;
	
	@Autowired
	private ProduitService produitService;
	
	@Inject
	private PanierBean panierBean;
	
	
	
	// chargement de tous les ordinateurs (portables / ordi bureau
	public String chargerPortables() {
		
		listOrdiBureau = new ArrayList<Produit>();
		
		listPortables = produitService.trierMoinsCherAuPlusCher(9L);
		
		return "achatguideusage.xhtml?faces-redirect=true&includeViewParams=true";
	}
	
	public String chargerOrdiBureau() {
		listPortables = new ArrayList<Produit>();
		
		listOrdiBureau = produitService.trierMoinsCherAuPlusCher(8L);
		return "achatguideusage.xhtml?faces-redirect=true&includeViewParams=true";
	}
	
	
	// filtrage par usage
	public String filtrerUsageOccasionnel () {
		if(listOrdiBureau.isEmpty()) {
			
			String typeOrdi = "ordi_port_usage";
			listFiltree  = new ArrayList<Produit>();
			
			listFiltree = produitService.filtrerUsageOccasionnel(listPortables, typeOrdi);
			
		} else {
			String typeOrdi = "ordi_bur_usage";
			listFiltree  = new ArrayList<Produit>();
			
			listFiltree = produitService.filtrerUsageOccasionnel(listOrdiBureau, typeOrdi);

		}

		return "achatguidebudget.xhtml?faces-redirect=true";
	}
	
	
	public String filtrerUsageRegulier () {
		if(listOrdiBureau.isEmpty()) {
			String typeOrdi = "ordi_port_usage";
			listFiltree  = new ArrayList<Produit>();
			
			listFiltree = produitService.filtrerUsageRegulier(listPortables, typeOrdi);
			
		} else {
			String typeOrdi = "ordi_bur_usage";
			listFiltree  = new ArrayList<Produit>();
			
			listFiltree = produitService.filtrerUsageRegulier(listOrdiBureau, typeOrdi);
		}

		return "achatguidebudget.xhtml?faces-redirect=true";
	}
	
	
	public String filtrerUsageIntensif () {
		if(listOrdiBureau.isEmpty()) {
			String typeOrdi = "ordi_port_usage";
			listFiltree  = new ArrayList<Produit>();
						
			listFiltree = produitService.filtrerUsageIntensif(listPortables, typeOrdi);
			
		} else {
			String typeOrdi = "ordi_bur_usage";
			listFiltree  = new ArrayList<Produit>();
			
			listFiltree = produitService.filtrerUsageIntensif(listOrdiBureau, typeOrdi);
		}

		return "achatguidebudget.xhtml?faces-redirect=true";
	}
	
	
	
	// filtrage par prix
	public String filtrerPrixBas() {
		listFiltreeParPrix = new ArrayList<Produit>();
		for (Produit produit : listFiltree) {
			if (produit.getPrix() < 500) {
				listFiltreeParPrix.add(produit);
			}
		}
		return "achatguidesuggestion.xhtml?faces-redirect=true";
		
	}
	
	
	public String filtrerPrixMoyen() {
		listFiltreeParPrix = new ArrayList<Produit>();
		for (Produit produit : listFiltree) {
			if (produit.getPrix() > 500 && produit.getPrix() < 1000) {
				listFiltreeParPrix.add(produit);
			}
		}
		return "achatguidesuggestion.xhtml?faces-redirect=true";
		
	}
	
	
	public String filtrerPrixHaut() {
		listFiltreeParPrix = new ArrayList<Produit>();
		for (Produit produit : listFiltree) {
			if (produit.getPrix() > 1000) {
				listFiltreeParPrix.add(produit);
			}
		}
		return "achatguidesuggestion.xhtml?faces-redirect=true";
	}
	

	public String afficherOverlayPlus(long typeProduitId) {
		
		overlayProduits = produitService.trierMoinsCherAuPlusCher(typeProduitId);
		
		overlayVisible = true;
		return null;
	}
	
	
	public String fermerOverlay() {
		overlayVisible = false;
		return null;
	}
	
	
	public String choisirProduit(Produit p) {
		fermerOverlay();
		panierBean.ajoutPanier(p);		
		System.out.println(" *** produit ADD ajouté : " + p.getType().getNom());
		if (p.getType().getId().equals(7L)){
			ecranIsSelected = true;
		}
		if (p.getType().getId().equals(6L)){
			clavierIsSelected = true;
		}
		if (p.getType().getId().equals(12L)){
			sourisIsSelected = true;
		}
		return null;
	}
	
	

	
}
