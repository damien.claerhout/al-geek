package fr.afcepf.al34.projetAL.web;

import java.io.Serializable;

import javax.annotation.ManagedBean;
import javax.enterprise.context.SessionScoped;

import org.springframework.beans.factory.annotation.Autowired;

import fr.afcepf.al34.projetAL.delegate.LocaleDelegate;
import fr.afcepf.al34.projetAL.delegate.ProduitDelegate;
import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.service.LocaleService;
import fr.afcepf.al34.projetAL.service.ProduitService;
import lombok.Getter;
import lombok.Setter;

@ManagedBean
@SessionScoped
public class FicheProduitBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter @Setter
	private long idProduit;
	
	@Getter @Setter
	private Produit produit;
	
	@Getter @Setter
	private int quantite;
	
	@Autowired
	private ProduitService prodService;

	@Autowired
	private LocaleService localeService;
	
//	@Autowired
//	private LocaleDelegate localeDelegate;
	
	public String afficherProduit(Long idProduit) {
		
		produit = prodService.getProduitAvecCaracteristiques(idProduit);
		
		return "ficheproduit.xhtml?faces-redirect=true";
	}
	
	public String traduireCaracteristique(String cle) {
		return localeService.getLocaleString(cle);
		//return localeDelegate.getLocaleString(cle);
	}
	
}
