package fr.afcepf.al34.projetAL.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;

import fr.afcepf.al34.projetAL.delegate.ClientDelegate;
import fr.afcepf.al34.projetAL.delegate.CommandeDelegate;
import fr.afcepf.al34.projetAL.entity.Commande;
import fr.afcepf.al34.projetAL.service.CommandeService;
import lombok.Getter;
import lombok.Setter;

@ManagedBean
@ViewScoped
public class EspacePersoBean implements Serializable {

	private static final long serialVersionUID = 1L;


	@Getter @Setter
	private List<Commande> historique;
	
	@Getter @Setter
	private String msgModif;
	
	@Autowired
	private CommandeService commandeService;
	
	@Autowired
	private ClientDelegate clientDelegate;	
	
	@Autowired
	ConnectBean connectBean;
	
	
	
	@PostConstruct
	public void init(){
		
		historique = commandeService.getCommandesPourClient(connectBean.getClient().getId());
		
	}
	
	public void modifInfosClient() {
		
		clientDelegate.modifier(connectBean.getClient());
		
		msgModif = "Modifications enregistrées";
	}
	
}
