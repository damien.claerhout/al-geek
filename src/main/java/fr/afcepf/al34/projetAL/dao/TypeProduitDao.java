package fr.afcepf.al34.projetAL.dao;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.projetAL.entity.TypeProduit;

public interface TypeProduitDao extends CrudRepository<TypeProduit, Long> {

	TypeProduit findByNom(String nom);
	
}
