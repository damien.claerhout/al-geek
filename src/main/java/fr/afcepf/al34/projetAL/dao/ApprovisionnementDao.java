package fr.afcepf.al34.projetAL.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.projetAL.entity.Approvisionnement;
import fr.afcepf.al34.projetAL.entity.Fournisseur;
import fr.afcepf.al34.projetAL.entity.Stock;




public interface ApprovisionnementDao extends CrudRepository<Approvisionnement, Long> {
	
	public List<Approvisionnement> findByStock(Stock stock);
	public List<Approvisionnement> findByFournisseur(Fournisseur fournisseur);

}
