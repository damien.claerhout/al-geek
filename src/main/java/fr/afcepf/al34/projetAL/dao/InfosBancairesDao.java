package fr.afcepf.al34.projetAL.dao;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.projetAL.entity.InfosBancaires;

public interface InfosBancairesDao extends CrudRepository<InfosBancaires, Long>{

}
