package fr.afcepf.al34.projetAL.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.entity.TypeProduit;

public interface ProduitDao extends CrudRepository<Produit, Long> {
	public List<Produit> findByType(TypeProduit type);
	public List<Produit> findByTypeId(Long id);
	
	public List<Produit> findTop30ByOrderByDateAjoutDesc();
	
	// test methode get produit filtré par prix - +
	public List<Produit> findByTypeOrderByPrixAsc(Long idType);
	 

}
