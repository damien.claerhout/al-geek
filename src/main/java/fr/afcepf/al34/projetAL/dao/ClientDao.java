package fr.afcepf.al34.projetAL.dao;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.projetAL.entity.Client;

public interface ClientDao extends CrudRepository<Client, Long> {
	
	public Client findByEmail(String email);


}
