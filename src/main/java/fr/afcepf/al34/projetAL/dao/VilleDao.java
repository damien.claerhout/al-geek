package fr.afcepf.al34.projetAL.dao;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.projetAL.entity.Ville;




public interface VilleDao extends CrudRepository<Ville, Long> {
	

}
