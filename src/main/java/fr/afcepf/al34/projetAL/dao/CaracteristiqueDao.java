package fr.afcepf.al34.projetAL.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.projetAL.entity.Caracteristique;
import fr.afcepf.al34.projetAL.entity.Produit;

public interface CaracteristiqueDao extends CrudRepository<Caracteristique, Long> {
	
	List<Caracteristique> findByProduit(Produit p);

}
