package fr.afcepf.al34.projetAL.dao;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.projetAL.entity.Fournisseur;



public interface FournisseurDao extends CrudRepository<Fournisseur, Long> {
	


}
