package fr.afcepf.al34.projetAL.dao;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.projetAL.entity.Categorie;

public interface CategorieDao extends CrudRepository<Categorie, Long> {

	Categorie findFirstByParentIsNull();
}
