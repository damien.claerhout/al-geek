package fr.afcepf.al34.projetAL.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.projetAL.entity.PaiementDiffere;

public interface PaiementDiffereDao extends CrudRepository<PaiementDiffere, Long>{
	List<PaiementDiffere>findAllByClient(Long clientId);

}
