package fr.afcepf.al34.projetAL.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.entity.Stock;



public interface StockDao extends CrudRepository<Stock, Long> {
	
	Stock findByProduit(Produit produit);
	
	@Query("select s from Stock s JOIN s.produit p WHERE p.type.id= ?1")
	public List<Stock> getByTypeProduit (long typeProduitId);
	
	@Query("SELECT stock from Stock stock WHERE stock.quantiteDispoPhysique <= ?1 ORDER BY stock.quantiteDispoPhysique")
	public List<Stock> getByQuantiteDispo(int quantite);
}
