package fr.afcepf.al34.projetAL.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.projetAL.entity.Commande;

public interface CommandeDao extends CrudRepository<Commande, Long> {
	
	List<Commande> findAllByClientIdOrderByDateCreationDesc(Long clientId);
	
	List<Commande> findByDateExpeditionIsNull();
	
	List<Commande> findByDateCreation(Date date);

}
