package fr.afcepf.al34.projetAL.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.projetAL.entity.LigneCommande;

public interface LigneCommandeDao extends CrudRepository<LigneCommande, Long> {
	
	@Query("SELECT lc from LigneCommande lc WHERE lc.commande.id = :commandeId")
	public List<LigneCommande> getByCommande (Long commandeId);

}
