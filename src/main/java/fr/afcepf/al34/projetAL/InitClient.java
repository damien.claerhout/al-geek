package fr.afcepf.al34.projetAL;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import fr.afcepf.al34.projetAL.dao.ClientDao;
import fr.afcepf.al34.projetAL.dao.CommandeDao;
import fr.afcepf.al34.projetAL.dao.InfosBancairesDao;
import fr.afcepf.al34.projetAL.dao.LigneCommandeDao;
import fr.afcepf.al34.projetAL.dao.ProduitDao;
import fr.afcepf.al34.projetAL.entity.Client;
import fr.afcepf.al34.projetAL.entity.Commande;
import fr.afcepf.al34.projetAL.entity.InfosBancaires;
import fr.afcepf.al34.projetAL.entity.LigneCommande;
import fr.afcepf.al34.projetAL.entity.Produit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Component
@Profile("initData")
@Getter @Setter @NoArgsConstructor
public class InitClient {
	
	@Autowired
	private ClientDao dao;

	@Autowired
	private LigneCommandeDao ligneCommandeDao;
	
	@Autowired
	private CommandeDao commandeDao;
	
	@Autowired
	private InfosBancairesDao infosBankDao;
	
	@Autowired
	private ProduitDao produitDao;
	
	private List<Produit> tousLesProduits;
	
	@PostConstruct
	public void init() {
		Client cl = new Client();
		cl.setNom("Bazaou");
		cl.setPrenom("Zakariya");
		cl.setEmail("zak78@gmail.com");
		cl.setHashedPassword("34341b341b7d341b7dfb341b7dfb70341b7dfb701d341b7dfb701d64341b7dfb701d649c341b7dfb701d649c50341b7dfb701d649c502b341b7dfb701d649c502bb7341b7dfb701d649c502bb752341b7dfb701d649c502bb752f5341b7dfb701d649c502bb752f5c1341b7dfb701d649c502bb752f5c1e9341b7dfb701d649c502bb752f5c1e9d4341b7dfb701d649c502bb752f5c1e9d4f7341b7dfb701d649c502bb752f5c1e9d4f723341b7dfb701d649c502bb752f5c1e9d4f723e7341b7dfb701d649c502bb752f5c1e9d4f723e7a6341b7dfb701d649c502bb752f5c1e9d4f723e7a636341b7dfb701d649c502bb752f5c1e9d4f723e7a63613341b7dfb701d649c502bb752f5c1e9d4f723e7a636138e341b7dfb701d649c502bb752f5c1e9d4f723e7a636138ed7341b7dfb701d649c502bb752f5c1e9d4f723e7a636138ed767341b7dfb701d649c502bb752f5c1e9d4f723e7a636138ed767e9341b7dfb701d649c502bb752f5c1e9d4f723e7a636138ed767e9c3341b7dfb701d649c502bb752f5c1e9d4f723e7a636138ed767e9c31e341b7dfb701d649c502bb752f5c1e9d4f723e7a636138ed767e9c31e41341b7dfb701d649c502bb752f5c1e9d4f723e7a636138ed767e9c31e41de341b7dfb701d649c502bb752f5c1e9d4f723e7a636138ed767e9c31e41dea6341b7dfb701d649c502bb752f5c1e9d4f723e7a636138ed767e9c31e41dea68e");
		cl.setAdresse("25 rue henri ");
		cl.setSalt("70UoxKScoHALwfETkBWW");
		cl.setVille("Saint Germain");
		cl.setCodePostal("78100");
		cl.setNumeroFixe("01.02.05.40.55");
		cl.setNumeroPort("06.08.57.03.09");
		
		
		dao.save(cl);
		
		Client cl2 = new Client();
		cl2.setNom("Messi");
		cl2.setPrenom("Lionel");
		cl2.setEmail("messi@gmail.com");
		cl2.setHashedPassword("7676d676d6f276d6f24776d6f2472f76d6f2472fdd76d6f2472fdd8576d6f2472fdd853976d6f2472fdd8539bd76d6f2472fdd8539bd0776d6f2472fdd8539bd07da76d6f2472fdd8539bd07da5976d6f2472fdd8539bd07da599676d6f2472fdd8539bd07da59968a76d6f2472fdd8539bd07da59968afb76d6f2472fdd8539bd07da59968afbb076d6f2472fdd8539bd07da59968afbb02676d6f2472fdd8539bd07da59968afbb0260c76d6f2472fdd8539bd07da59968afbb0260cce76d6f2472fdd8539bd07da59968afbb0260cce9576d6f2472fdd8539bd07da59968afbb0260cce95ce76d6f2472fdd8539bd07da59968afbb0260cce95ce1476d6f2472fdd8539bd07da59968afbb0260cce95ce141c76d6f2472fdd8539bd07da59968afbb0260cce95ce141c8276d6f2472fdd8539bd07da59968afbb0260cce95ce141c829876d6f2472fdd8539bd07da59968afbb0260cce95ce141c82982276d6f2472fdd8539bd07da59968afbb0260cce95ce141c829822dd76d6f2472fdd8539bd07da59968afbb0260cce95ce141c829822dd8376d6f2472fdd8539bd07da59968afbb0260cce95ce141c829822dd839576d6f2472fdd8539bd07da59968afbb0260cce95ce141c829822dd83956176d6f2472fdd8539bd07da59968afbb0260cce95ce141c829822dd8395616e76d6f2472fdd8539bd07da59968afbb0260cce95ce141c829822dd8395616ec4");
		cl2.setAdresse("5 rue de la grande place");
		cl2.setSalt("R0GQmN8xxZR3xSr0ByXL");
		cl2.setVille("Paris");
		cl2.setCodePostal("75018");
		cl2.setNumeroFixe("01.38.23.68.74");
		cl2.setNumeroPort("06.08.57.03.09");
		
		dao.save(cl2);
		
		Client cl3 = new Client();
		cl3.setNom("Ronaldo");
		cl3.setPrenom("Cristiano");
		cl3.setEmail("ronaldo@gmail.com");
		cl3.setHashedPassword("1c1c4e1c4e981c4e98351c4e9835ab1c4e9835ab4f1c4e9835ab4ff31c4e9835ab4ff3491c4e9835ab4ff349691c4e9835ab4ff34969211c4e9835ab4ff3496921221c4e9835ab4ff3496921222a1c4e9835ab4ff3496921222a811c4e9835ab4ff3496921222a819b1c4e9835ab4ff3496921222a819b191c4e9835ab4ff3496921222a819b192f1c4e9835ab4ff3496921222a819b192f5e1c4e9835ab4ff3496921222a819b192f5e011c4e9835ab4ff3496921222a819b192f5e01451c4e9835ab4ff3496921222a819b192f5e0145721c4e9835ab4ff3496921222a819b192f5e014572b41c4e9835ab4ff3496921222a819b192f5e014572b45b1c4e9835ab4ff3496921222a819b192f5e014572b45b351c4e9835ab4ff3496921222a819b192f5e014572b45b35b81c4e9835ab4ff3496921222a819b192f5e014572b45b35b88a1c4e9835ab4ff3496921222a819b192f5e014572b45b35b88a0e1c4e9835ab4ff3496921222a819b192f5e014572b45b35b88a0ee61c4e9835ab4ff3496921222a819b192f5e014572b45b35b88a0ee68e1c4e9835ab4ff3496921222a819b192f5e014572b45b35b88a0ee68e9a1c4e9835ab4ff3496921222a819b192f5e014572b45b35b88a0ee68e9adb1c4e9835ab4ff3496921222a819b192f5e014572b45b35b88a0ee68e9adbe51c4e9835ab4ff3496921222a819b192f5e014572b45b35b88a0ee68e9adbe57f");
		cl3.setAdresse("10 rue du Stade de France");
		cl3.setSalt("L2pxS3loiZoqI50wYmv9");
		cl3.setVille("Saint Denis");
		cl3.setCodePostal("93200");
		cl3.setNumeroFixe("01.02.05.40.55");
		cl3.setNumeroPort("06.28.48.37.18");
		
		dao.save(cl3);
		

		// client test demo 
		Client cl4 = new Client();
		cl4.setNom("Dupont");
		cl4.setPrenom("Arthur");
		cl4.setEmail("adupont@gmail.com");
		cl4.setHashedPassword("fcfca3fca39afca39a22fca39a22abfca39a22abfcfca39a22abfc24fca39a22abfc24c0fca39a22abfc24c0d5fca39a22abfc24c0d5c9fca39a22abfc24c0d5c924fca39a22abfc24c0d5c92460fca39a22abfc24c0d5c924605efca39a22abfc24c0d5c924605ec7fca39a22abfc24c0d5c924605ec70bfca39a22abfc24c0d5c924605ec70b48fca39a22abfc24c0d5c924605ec70b48a8fca39a22abfc24c0d5c924605ec70b48a89ffca39a22abfc24c0d5c924605ec70b48a89fc3fca39a22abfc24c0d5c924605ec70b48a89fc383fca39a22abfc24c0d5c924605ec70b48a89fc38342fca39a22abfc24c0d5c924605ec70b48a89fc3834233fca39a22abfc24c0d5c924605ec70b48a89fc383423379fca39a22abfc24c0d5c924605ec70b48a89fc383423379acfca39a22abfc24c0d5c924605ec70b48a89fc383423379acf0fca39a22abfc24c0d5c924605ec70b48a89fc383423379acf014fca39a22abfc24c0d5c924605ec70b48a89fc383423379acf01466fca39a22abfc24c0d5c924605ec70b48a89fc383423379acf0146626fca39a22abfc24c0d5c924605ec70b48a89fc383423379acf01466260bfca39a22abfc24c0d5c924605ec70b48a89fc383423379acf01466260b94fca39a22abfc24c0d5c924605ec70b48a89fc383423379acf01466260b94c9fca39a22abfc24c0d5c924605ec70b48a89fc383423379acf01466260b94c98f");
		// mdp : azerty
		cl4.setAdresse("25 rue Marsoulan");
		cl4.setSalt("kUjjnFW0xB2clcgumPKm");
		cl4.setVille("Paris");
		cl4.setCodePostal("75012");
		cl4.setNumeroFixe("01.85.85.58.23");
		cl4.setNumeroPort("06.85.85.58.55");
		dao.save(cl4);
		
		
		// Commandes
		tousLesProduits = (List<Produit>) produitDao.findAll();
		
		Random random = new Random();
		commandeAleatoire(random, cl);
		commandeAleatoire(random, cl);
		commandeAleatoire(random, cl2);
		commandeAleatoire(random, cl2);
		commandeAleatoire(random, cl3);
		commandeAleatoire(random, cl3);
		commandeAleatoire(random, cl4);
		commandeAleatoire(random, cl4);
//		commandeAleatoireNonTraitee(random, cl);
//		commandeAleatoireNonTraitee(random, cl2);
//		commandeAleatoireNonTraitee(random, cl3);
//		commandeAleatoireNonTraitee(random, cl4);
		
	}

	private void commandeAleatoire(Random random, Client cl) {
		int numLignes = random.nextInt(3) + 1;
		ArrayList<LigneCommande> lignes = new ArrayList<LigneCommande>();
		
		double prixCmd = 0;
		Date date = randomDate(random);
		
		Commande cmd = new Commande(null, null, date, 0D);
		commandeDao.save(cmd);
		
		DecimalFormat twoDForm =new DecimalFormat("##.##");
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator('.');
		twoDForm.setDecimalFormatSymbols(dfs);
		
		for (int i = 0; i < numLignes; ++i) {
			LigneCommande ligne = new LigneCommande(null, random.nextInt(3) + 1, produitAleatoire(random));
			lignes.add(ligne);
			ligne.setCommande(cmd);
			
			double prixLigne = Double.parseDouble(twoDForm.format(ligne.getQuantite() * ligne.getProduit().getPrix()));
			prixCmd += prixLigne;
			
			ligne.setPrixTotal(prixLigne);
			ligneCommandeDao.save(ligne);
		}
		
		cmd.setDateExpedition(date);
		cmd.setPrix(Double.parseDouble(twoDForm.format(prixCmd)));
		cmd.setListLigneCommande(lignes);
				
		cmd.setClient(cl);
		
		InfosBancaires infosBank = new InfosBancaires();
		infosBank.setCryptogramme("999");
		infosBank.setDateCarte(randomDate(random));
		
		cmd.setInfosBank(infosBank);
		infosBankDao.save(infosBank);
		
		commandeDao.save(cmd);
		
	}
	
	private void commandeAleatoireNonTraitee(Random random, Client cl) {
		int numLignes = random.nextInt(3) + 1;
		ArrayList<LigneCommande> lignes = new ArrayList<LigneCommande>();
		
		double prixCmd = 0;
		Date date = new Date();
		
		Commande cmd = new Commande(null, null, date, 0D);
		commandeDao.save(cmd);
		
		DecimalFormat twoDForm =new DecimalFormat("##.##");
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator('.');
		twoDForm.setDecimalFormatSymbols(dfs);
		
		for (int i = 0; i < numLignes; ++i) {
			LigneCommande ligne = new LigneCommande(null, random.nextInt(3) + 1, produitAleatoire(random));
			lignes.add(ligne);
			ligne.setCommande(cmd);
			
			double prixLigne = Double.parseDouble(twoDForm.format(ligne.getQuantite() * ligne.getProduit().getPrix()));
			prixCmd += prixLigne;
			
			ligne.setPrixTotal(prixLigne);
			ligneCommandeDao.save(ligne);
		}
		
		cmd.setPrix(Double.parseDouble(twoDForm.format(prixCmd)));
		cmd.setListLigneCommande(lignes);
				
		cmd.setClient(cl);
		
		InfosBancaires infosBank = new InfosBancaires();
		infosBank.setCryptogramme("999");
		infosBank.setDateCarte(randomDate(random));
		
		cmd.setInfosBank(infosBank);
		infosBankDao.save(infosBank);
		
		commandeDao.save(cmd);
		
	}

	private Produit produitAleatoire(Random random) {
		int idx = random.nextInt(tousLesProduits.size());
		return tousLesProduits.get(idx);
	}
	
	private Date randomDate(Random random) {
		int annee = 2018 + random.nextInt(1);
		int mois = random.nextInt(12);
		int jour = random.nextInt(27) + 1;
		return new GregorianCalendar(annee, mois, jour).getTime();
	}
}
