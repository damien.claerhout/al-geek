package fr.afcepf.al34.projetAL.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class DepanneurDto {

	private Long id;
	
	private String nomSociete;
	
	private String adresse;

	private String ville;
	
	private double lat;
	
	private double lon;
	
	private String telephone;
	
	private String email;

}
