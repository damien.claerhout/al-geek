package fr.afcepf.al34.projetAL.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class PaiementDiffereDto {
	
	private double somme;
	private int mois;
	private double mensualite;

}
