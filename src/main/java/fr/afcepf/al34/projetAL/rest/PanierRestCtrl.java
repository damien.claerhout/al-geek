package fr.afcepf.al34.projetAL.rest;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.projetAL.entity.Panier;
import fr.afcepf.al34.projetAL.entity.ProduitPanier;
import fr.afcepf.al34.projetAL.service.PanierService;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/algeek-api/public/panier" , headers="Accept=application/json")
public class PanierRestCtrl {
	
	@Autowired
	private PanierService panierService;
	
	//localhost:8080/projetAL/algeek-api/public/panier/ajout
	@PostMapping(value="/ajout")
	public Panier ajoutPanier(@RequestBody ProduitPanier produitPanier) {

		return panierService.ajoutPanier(produitPanier.getPanier(), produitPanier.getProduit());
	}
	
	//localhost:8080/projetAL/algeek-api/public/panier/retrait
	@PostMapping(value="/retrait")
	public Panier retirerProduitDuPanier(@RequestBody ProduitPanier produitPanier) {
		System.out.println(" *** CRTL : tentative de retrait de " + produitPanier.getProduit().getNom());
		return panierService.retirerProduitDuPanier(produitPanier.getPanier(), produitPanier.getProduit());
	}

}
