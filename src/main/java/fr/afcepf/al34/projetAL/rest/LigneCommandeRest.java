package fr.afcepf.al34.projetAL.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.projetAL.entity.LigneCommande;
import fr.afcepf.al34.projetAL.service.LigneCommandeService;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/algeek-api/public/ligne-commande" , headers="Accept=application/json")
public class LigneCommandeRest {
	
	@Autowired
	private LigneCommandeService ligneCommandeService;
	
	//localhost:8080/projetAL/algeek-api/public/ligne-commande/add
	@PostMapping(value="/add")
	public LigneCommande ajouter(@RequestBody LigneCommande t) {
		return ligneCommandeService.ajouter(t);
	}
	
	//localhost:8080/projetAL/algeek-api/public/ligne-commande/get-by-commande?commandeId=5
	@GetMapping(value="/get-by-commande")
	List<LigneCommande> getByCommmande(@RequestParam(value="commandeId") Long commandeId) {
		return ligneCommandeService.getByCommande(commandeId);	
	}
	
	//localhost:8080/projetAL/algeek-api/public/ligne-commande
	@DeleteMapping(value="")
	public boolean supprimer (@RequestBody LigneCommande t) {
    	return ligneCommandeService.supprimer(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/ligne-commande
	@PutMapping(value="")
	public LigneCommande modifier(@RequestBody LigneCommande t) {
    	return ligneCommandeService.modifier(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/ligne-commande?id=1
	@GetMapping(value="")
	public LigneCommande rechercherParId (@RequestParam(value="id") Long id) {
    	return ligneCommandeService.rechercherParId(id);
    }
    
	//localhost:8080/projetAL/algeek-api/public/ligne-commande/all
	@GetMapping(value="/all")
	public List<LigneCommande> getAll(){
    	return ligneCommandeService.getAll();
    }

}
