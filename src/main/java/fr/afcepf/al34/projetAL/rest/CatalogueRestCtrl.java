package fr.afcepf.al34.projetAL.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.service.CatalogueService;



@RestController
@CrossOrigin("*")
@RequestMapping(value="/algeek-api/public/catalogue" , headers="Accept=application/json")
public class CatalogueRestCtrl{
	
	@Autowired
	private CatalogueService catalogueService;
	
	//localhost:8080/projetAL/algeek-api/public/catalogue/meilleuresVentes?numResultatsMax=10
	@GetMapping(value="/meilleuresVentes")
	public List<Produit> getMeilleuresVentes(@RequestParam(value="numResultatsMax") int numResultatsMax) {
		return catalogueService.getMeilleuresVentes(numResultatsMax);
	}

	//localhost:8080/projetAL/algeek-api/public/catalogue/nouveautes
	@GetMapping(value="/nouveautes")
	public List<Produit> getNouveautes() {
		
		return catalogueService.getNouveautes();		
	}
	
	//localhost:8080/projetAL/algeek-api/public/catalogue/add
	@PostMapping(value="/add")
	public Produit ajouter(@RequestBody Produit t) {
		return catalogueService.ajouter(t);
	}
	
	//localhost:8080/projetAL/algeek-api/public/catalogue
	@DeleteMapping(value="")
	public boolean supprimer (@RequestBody Produit t) {
    	return catalogueService.supprimer(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/catalogue
	@PutMapping(value="")
	public Produit modifier(@RequestBody Produit t) {
    	return catalogueService.modifier(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/catalogue?id=1
	@GetMapping(value="")
	public Produit rechercherParId (@RequestParam(value="id") Long id) {
    	return catalogueService.rechercherParId(id);
    }
    
	//localhost:8080/projetAL/algeek-api/public/catalogue/all
	@GetMapping(value="/all")
	public List<Produit> getAll(){
    	return catalogueService.getAll();
    }
}
