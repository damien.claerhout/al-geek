package fr.afcepf.al34.projetAL.rest;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.projetAL.entity.Categorie;
import fr.afcepf.al34.projetAL.service.CategorieService;



@RestController
@CrossOrigin("*")
@RequestMapping(value="/algeek-api/public/categorie" , headers="Accept=application/json")
public class CategorieRestCtrl {
	
	@Autowired
	private CategorieService categorieService;
	
	//localhost:8080/projetAL/algeek-api/public/categorie/root
	@GetMapping(value="/root")
	public Categorie getRootCategorie() {
		return categorieService.getRootCategorie();
	}

		
	//localhost:8080/projetAL/algeek-api/public/categorie/add
	@PostMapping(value="/add")
	public Categorie ajouter(@RequestBody Categorie t) {
		return categorieService.ajouter(t);
	}
	
	//localhost:8080/projetAL/algeek-api/public/categorie
	@DeleteMapping(value="")
	public boolean supprimer (@RequestBody Categorie t) {
    	return categorieService.supprimer(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/categorie
	@PutMapping(value="")
	public Categorie modifier(@RequestBody Categorie t) {
    	return categorieService.modifier(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/categorie?id=5
	@GetMapping(value="")
	public Categorie rechercherParId (@RequestParam(value="id") Long id) {
    	return categorieService.rechercherParId(id);
    }
    
	//localhost:8080/projetAL/algeek-api/public/categorie/all
	@GetMapping(value="/all")
	public List<Categorie> getAll(){
    	return categorieService.getAll();
    }

}
