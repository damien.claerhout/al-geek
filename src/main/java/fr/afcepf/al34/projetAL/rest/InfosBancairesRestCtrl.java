package fr.afcepf.al34.projetAL.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.projetAL.entity.InfosBancaires;
import fr.afcepf.al34.projetAL.service.InfosBancairesService;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/algeek-api/public/infos-bancaires" , headers="Accept=application/json")
public class InfosBancairesRestCtrl {
	
	@Autowired
	private InfosBancairesService infosBancairesService;
	
	//localhost:8080/projetAL/algeek-api/public/infos-bancaires/add
	@PostMapping(value="/add")
	public InfosBancaires ajouter(@RequestBody InfosBancaires t) {
		return infosBancairesService.ajouter(t);
	}
	
	//localhost:8080/projetAL/algeek-api/public/infos-bancaires
	@DeleteMapping(value="")
	public boolean supprimer (@RequestBody InfosBancaires t) {
    	return infosBancairesService.supprimer(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/infos-bancaires
	@PutMapping(value="")
	public InfosBancaires modifier(@RequestBody InfosBancaires t) {
    	return infosBancairesService.modifier(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/infos-bancaires?id=1
	@GetMapping(value="")
	public InfosBancaires rechercherParId (@RequestParam(value="id") Long id) {
    	return infosBancairesService.rechercherParId(id);
    }
    
	//localhost:8080/projetAL/algeek-api/public/infos-bancaires/all
	@GetMapping(value="/all")
	public List<InfosBancaires> getAll(){
    	return infosBancairesService.getAll();
    }

}
