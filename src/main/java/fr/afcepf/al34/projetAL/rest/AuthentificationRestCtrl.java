package fr.afcepf.al34.projetAL.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.projetAL.entity.CredentialsInit;
import fr.afcepf.al34.projetAL.service.AuthentificationService;
import fr.afcepf.al34.projetAL.service.authentification.exception.AuthentificationException;



@RestController
@CrossOrigin("*")
@RequestMapping(value="/algeek-api/public/authentification" , headers="Accept=application/json")
public class AuthentificationRestCtrl{
	

	@Autowired
	private AuthentificationService authentificationService;
	
	//localhost:8080/projetAL/algeek-api/public/authentification/init
	@PostMapping(value="/init")
	public CredentialsInit initializeCredentials(@RequestBody CredentialsInit  credentialsInit) throws AuthentificationException {
		
		
		authentificationService.initializeCredentials(credentialsInit.getCredentials(), credentialsInit.getClearPassword());
		
		return credentialsInit;
	}


	//localhost:8080/projetAL/algeek-api/public/authentification/
	@PostMapping(value="")
	public boolean authentificate(@RequestBody CredentialsInit  credentialsInit) throws AuthentificationException {
		return authentificationService.authentificate(credentialsInit.getCredentials(), credentialsInit.getClearPassword());
	}
	


}
