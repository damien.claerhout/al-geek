package fr.afcepf.al34.projetAL.rest;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.entity.Stock;
import fr.afcepf.al34.projetAL.service.StockService;



@RestController
@CrossOrigin("*")
@RequestMapping(value="/algeek-api//public/stock" , headers="Accept=application/json")
public class StockRestCtrl {
	
	@Autowired
	private StockService stockService;

	//localhost:8080/projetAL/algeek-api/public/stock/get-by-type?id=1
	@GetMapping(value="/get-by-type")
	public List<Stock> getByTypeProduit (@RequestParam(value="id")  Long typeProduitId) {
		return stockService.getByTypeProduit(typeProduitId);
	}

	//localhost:8080/projetAL/algeek-api/public/stock/get-by-produit
	@PostMapping(value="/get-by-produit")
	public Stock getByProduit(@RequestBody Produit produit) {
		return stockService.getByProduit(produit);
	}

	//localhost:8080/projetAL/algeek-api/public/stock/get-by-quantite-dispo
	@GetMapping(value="/get-by-quantite")
	public List<Stock> getByQuantiteDispo(int quantite) {
		
		return stockService.getByQuantiteDispo(quantite);
	}
	
	//localhost:8080/projetAL/algeek-api/public/stock/add
	@PostMapping(value="/add")
	public Stock ajouter(@RequestBody Stock t) {
		return stockService.ajouter(t);
	}

	//localhost:/projetAL/algeek-api/public/stock
	@DeleteMapping(value="")
	public boolean supprimer (@RequestBody Stock t) {
    	return stockService.supprimer(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/stock
	@PutMapping(value="")
	public Stock modifier(@RequestBody Stock t) {
    	return stockService.modifier(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/stock?id=1
	@GetMapping(value="")
	public Stock rechercherParId (@RequestParam(value="id") Long id) {
    	return stockService.rechercherParId(id);
    }
    
	//localhost:8080/projetAL/algeek-api/public/stock/all
	@GetMapping(value="/all")
	public List<Stock> getAll(){
    	return stockService.getAll();
    }
	


}
