package fr.afcepf.al34.projetAL.rest;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.projetAL.service.LocaleService;


@RestController
@CrossOrigin("*")
@RequestMapping(value="/algeek-api/public/locale" , headers="Accept=application/json")
public class LocaleCtrl {
	
	@Autowired
	private LocaleService localeService;
	
	@GetMapping(value="")
	public String getLocaleString(@RequestParam(value="cle") String	cle) {

		return localeService.getLocaleString(cle);
	}


}
