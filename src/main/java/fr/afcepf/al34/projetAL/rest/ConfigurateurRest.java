package fr.afcepf.al34.projetAL.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.projetAL.entity.ComparaisonProduits;
import fr.afcepf.al34.projetAL.entity.TypeProduit;
import fr.afcepf.al34.projetAL.service.ConfigurateurService;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/algeek-api/public/configurateur" , headers="Accept=application/json")
public class ConfigurateurRest {

	
	@Autowired
	private ConfigurateurService configurateurService;
	
	
	//localhost:8080/projetAL/algeek-api/public/configurateur/composants
	@GetMapping(value="/composants")
	public List<TypeProduit> getTypesComposants() {
		
		return configurateurService.getTypesComposants();
	}
	
	//localhost:8080/projetAL/algeek-api/public/configurateur/compatibilite
	@PostMapping(value="/compatibilite")
	public boolean estCompatibleAvec(@RequestBody ComparaisonProduits comparaisonProduit) {
		
		return configurateurService.estCompatibleAvec(comparaisonProduit.getPremierProduit(), comparaisonProduit.getSecondProduit());
	}
}
