package fr.afcepf.al34.projetAL.rest;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import fr.afcepf.al34.projetAL.entity.Client;
import fr.afcepf.al34.projetAL.entity.LoginRequest;
import fr.afcepf.al34.projetAL.service.ClientService;




@RestController
@CrossOrigin("*")
@RequestMapping(value="/algeek-api/public/client" , headers="Accept=application/json")
public class ClientRestCtrl {
	
	@Autowired
	private ClientService clientService;
	
	//localhost:8080/projetAL/algeek-api/public/client/login
	@PostMapping(value="/login")
	public Client doConnecter(@RequestBody LoginRequest loginRequest) {
		return clientService.doConnecter(loginRequest.getUsername(), loginRequest.getPassword());
	}
	
	@PostMapping(value="/get-email")
	public Client findByEmail(@RequestBody String email) {
		return clientService.findByEmail(email);
	}


	//localhost:8080/projetAL/algeek-api/public/client/add
	@PostMapping(value="/add")
	public Client ajouter(@RequestBody Client t) {
		return clientService.ajouter(t);
	}

	//localhost:8080/projetAL/algeek-api/public/client
	@DeleteMapping(value="")
	public boolean supprimer (@RequestBody Client t) {
    	return clientService.supprimer(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/client
	@PutMapping(value="")
	public Client modifier(@RequestBody Client t) {
    	return clientService.modifier(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/client?id=1
	@GetMapping(value="")
	public Client rechercherParId (@RequestParam(value="id") Long id) {
    	return clientService.rechercherParId(id);
    }
    
	//localhost:8080/projetAL/algeek-api/public/client/all
	@GetMapping(value="/all")
	public List<Client> getAll(){
    	return clientService.getAll();
    }

}
