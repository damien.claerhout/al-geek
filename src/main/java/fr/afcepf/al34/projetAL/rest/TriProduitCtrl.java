package fr.afcepf.al34.projetAL.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.service.TriProduitsService;



@RestController
@CrossOrigin("*")
@RequestMapping(value="/algeek-api/public/tri" , headers="Accept=application/json")
public class TriProduitCtrl {

	@Autowired
	TriProduitsService triProduitsService;
	
	
	//localhost:8080/projetAL/algeek-api/public/tri/marque-croissant
	@PostMapping(value="/marque-croissant")
	public List<Produit> trierParMarqueCroissant(@RequestBody List<Produit> produits) {		
		return triProduitsService.trierParMarqueCroissant(produits);		
	}

	//localhost:8080/projetAL/algeek-api/public/
	@PostMapping(value="/marque-decroissant")
	public List<Produit> trierParMarqueDecroissant(@RequestBody List<Produit> produits) {
		return triProduitsService.trierParMarqueCroissant(produits);
	}

	@PostMapping(value="/prix-croissant")
	public List<Produit> trierParPrixCroissant(@RequestBody List<Produit> produits) {
		return triProduitsService.trierParMarqueCroissant(produits);	
	}

	@PostMapping(value="/prix-decroissant")
	public List<Produit> trierParPrixDecroissant(@RequestBody List<Produit> produits) {
		return triProduitsService.trierParMarqueCroissant(produits);	
	}

	@PostMapping(value="/alphabetique")
	public List<Produit> trierParNomAlphabetique(@RequestBody List<Produit> produits) {
		return triProduitsService.trierParMarqueCroissant(produits);	
	}
}
