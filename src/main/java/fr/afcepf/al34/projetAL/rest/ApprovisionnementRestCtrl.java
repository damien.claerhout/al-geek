package fr.afcepf.al34.projetAL.rest;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



import fr.afcepf.al34.projetAL.entity.Approvisionnement;
import fr.afcepf.al34.projetAL.entity.Fournisseur;
import fr.afcepf.al34.projetAL.entity.Stock;
import fr.afcepf.al34.projetAL.service.ApprovisionnementService;



@RestController
@RequestMapping(value="/algeek-api//public/approvisionnement" , headers="Accept=application/json")
public class ApprovisionnementRestCtrl {
	
	@Autowired
	private ApprovisionnementService approvisionnementService;
	
	//localhost:8080/projetAL/algeek-api/public/approvisionnement/by-stock
	@PostMapping(value="/by-stock")
	public List<Approvisionnement> findByStock (@RequestBody Stock t) {
		return approvisionnementService.findByStock(t);
	}
	
	//localhost:8080/projetAL/algeek-api/public/approvisionnement/by-fournisseur
	@PostMapping(value="/by-fournisseur")
	public List<Approvisionnement> findByFournisseur (@RequestBody Fournisseur t) {
		return approvisionnementService.findByFournisseur(t);
	}
	
	//localhost:8080/algeek-admin/public/approvisionnement/add
	@PostMapping(value="/add")
	public Approvisionnement ajouter(@RequestBody Approvisionnement t) {
		return approvisionnementService.ajouter(t);
	}

	//localhost:8080/algeek-admin/public/approvisionnement
	@DeleteMapping(value="")
	public boolean supprimer (@RequestBody Approvisionnement t) {
    	return approvisionnementService.supprimer(t);
    }
    
	//localhost:8080/algeek-admin/public/approvisionnement
	@PutMapping(value="")
	public Approvisionnement modifier(@RequestBody Approvisionnement t) {
    	return approvisionnementService.modifier(t);
    }
    
	//localhost:8080/algeek-admin/public/approvisionnement?id=1
	@GetMapping(value="")
	public Approvisionnement rechercherParId (@RequestParam(value="id") Long id) {
    	return approvisionnementService.rechercherParId(id);
    }
    
	//localhost:8080/algeek-admin/public/approvisionnement/all
	@GetMapping(value="/all")
	public List<Approvisionnement> getAll(){
    	return approvisionnementService.getAll();
    }

}
