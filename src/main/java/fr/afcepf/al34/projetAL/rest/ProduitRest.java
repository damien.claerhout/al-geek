package fr.afcepf.al34.projetAL.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.entity.TypeProduit;
import fr.afcepf.al34.projetAL.service.ProduitService;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/algeek-api/public/produit" , headers="Accept=application/json")
public class ProduitRest {
	
	@Autowired
	private ProduitService produitService;
	
	//localhost:8080/projetAL/algeek-api/public/produit/all-types
	@GetMapping(value="/all-types")
	public List<TypeProduit> getTousTypesProduits() {
		return produitService.getTousTypesProduits();
	}
	
	//localhost:8080/projetAL/algeek-api/public/produit/caracteristique?id=1
	@GetMapping(value="/caracteristique")
	public Produit getProduitAvecCaracteristiques(@RequestParam(value="id") Long id) {
		return produitService.getProduitAvecCaracteristiques(id);
	}

	//localhost:8080/projetAL/algeek-api/public/produit/type?id=1&chargerCaracteristiques=true
	@GetMapping(value="/type")
	public List<Produit> getProduitsParType(@RequestParam(value="id") Long id, @RequestParam(value="chargerCaracteristiques") boolean chargerCaracteristiques) {
		return produitService.getProduitsParType(id, chargerCaracteristiques);		
	}
	
	
	//localhost:8080/projetAL/algeek-api/public/produit/filtrer-usage-occasionnel?typeOrdi="ordi_port_usage"
	@PostMapping(value="/filtrer-usage-occasionnel")
	public List<Produit> filtrerUsageOccasionnel(@RequestBody List<Produit> listP,  @RequestParam(value="typeOrdi") String typeOrdi) {
		return produitService.filtrerUsageOccasionnel(listP, typeOrdi);		
	}
	
	
	//localhost:8080/projetAL/algeek-api/public/produit/filtrer-usage-regulier?typeOrdi="ordi_bur_usage"
	@PostMapping(value="/filtrer-usage-regulier")
	public List<Produit> filtrerUsageRegulier(@RequestBody List<Produit> listP,  @RequestParam(value="typeOrdi")  String typeOrdi) {
		return produitService.filtrerUsageRegulier(listP, typeOrdi);		
	}
	
	
	//localhost:8080/projetAL/algeek-api/public/produit/filtrer-usage-intensif?typeOrdi="ordi_port_usage"
	@PostMapping(value="/filtrer-usage-intensif")
	public List<Produit> filtrerUsageIntensif(@RequestBody List<Produit> listP,  @RequestParam(value="typeOrdi") String typeOrdi) {
		return produitService.filtrerUsageIntensif(listP, typeOrdi);	
	}
	
	//localhost:8080/projetAL/algeek-api/public/produit/tri-du-plus-cher-au-moins-cher?idType=8
	@GetMapping(value="/tri-du-plus-cher-au-moins-cher")
	public List<Produit> trierMoinsCherAuPlusCher(@RequestParam(value="idType") Long idType) {
		return produitService.trierMoinsCherAuPlusCher(idType);
	}
	
	//localhost:8080/projetAL/algeek-api/public/produit/add
	@PostMapping(value="/add")
	public Produit ajouter(@RequestBody Produit t) {
		return produitService.ajouter(t);
	}
	
	//localhost:8080/projetAL/algeek-api/public/produit
	@DeleteMapping(value="")
	public boolean supprimer (@RequestBody Produit t) {
    	return produitService.supprimer(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/produit
	@PutMapping(value="")
	public Produit modifier(@RequestBody Produit t) {
    	return produitService.modifier(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/produit?id=1
	@GetMapping(value="")
	public Produit rechercherParId (@RequestParam(value="id") Long id) {
    	return produitService.rechercherParId(id);
    }
    
	//localhost:8080/projetAL/algeek-api/public/produit/all
	@GetMapping(value="/all")
	public List<Produit> getAll(){
    	return produitService.getAll();
    }

}
