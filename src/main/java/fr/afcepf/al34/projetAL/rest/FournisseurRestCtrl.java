package fr.afcepf.al34.projetAL.rest;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import fr.afcepf.al34.projetAL.entity.Fournisseur;
import fr.afcepf.al34.projetAL.service.FournisseurService;


@RestController
@CrossOrigin("*")
@RequestMapping(value="/algeek-api/public/fournisseur" , headers="Accept=application/json")
public class FournisseurRestCtrl {
	
	@Autowired
	private FournisseurService fournisseurService;
	



	//localhost:8080/projetAL/algeek-api/public/fournisseur/add
	@PostMapping(value="/add")
	public Fournisseur ajouter(@RequestBody Fournisseur t) {
		return fournisseurService.ajouter(t);
	}

	//localhost:8080/projetAL/algeek-api/public/fournisseur
	@DeleteMapping(value="")
	public boolean supprimer (@RequestBody Fournisseur t) {
    	return fournisseurService.supprimer(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/fournisseur
	@PutMapping(value="")
	public Fournisseur modifier(@RequestBody Fournisseur t) {
    	return fournisseurService.modifier(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/fournisseur?id=1
	@GetMapping(value="")
	public Fournisseur rechercherParId (@RequestParam(value="id") Long id) {
    	return fournisseurService.rechercherParId(id);
    }
    
	//localhost:8080/projetAL/algeek-api/public/fournisseur/all
	@GetMapping(value="/all")
	public List<Fournisseur> getAll(){
    	return fournisseurService.getAll();
    }

}
