package fr.afcepf.al34.projetAL.rest;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.projetAL.entity.Commande;
import fr.afcepf.al34.projetAL.service.CommandeService;





@RestController
@CrossOrigin("*")
@RequestMapping(value="/algeek-api/public/commande" , headers="Accept=application/json")
public class CommandeRestCtrl {
	
	@Autowired
	private CommandeService commandeService;
	
	private SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yy");
	
	//localhost:8080/projetAL/algeek-api/public/commande/date-du-jour
	@PostMapping(value="/valider-commande")
	public Commande validerCommande(@RequestBody Commande commande){
		
		return commandeService.validerCommande(commande);
	}	
	
	//localhost:8080/projetAL/algeek-api/public/commande/client?idClient=1
	@GetMapping(value="/client")
	public List<Commande> getCommandesPourClient(@RequestParam(value="idClient") Long idClient) {
		return commandeService.getCommandesPourClient(idClient);
	}
	
	//localhost:8080/projetAL/algeek-api/public/commande/commandes-a-traiter	
	@GetMapping(value="/commandes-a-traiter")
	public List<Commande> getCommandesAtraiter() {
		return commandeService.getCommandesAtraiter();
	}
	
	//localhost:8080/projetAL/algeek-api/public/commande/date-du-jour
	@PostMapping(value="/date-du-jour")
	public List<Commande> getCommandesDuJour(@RequestBody Date date){
		formater.format(date);
		return commandeService.findByDateCreation(date);
	}
	
	
	//localhost:8080/projetAL/algeek-api/public/commande/add
	@PostMapping(value="/add")
	public Commande ajouter(@RequestBody Commande t) {
		return commandeService.ajouter(t);
	}
	
	//localhost:8080/projetAL/algeek-api/public/commande
	@DeleteMapping(value="")
	public boolean supprimer (@RequestBody Commande t) {
    	return commandeService.supprimer(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/commande
	@PutMapping(value="")
	public Commande modifier(@RequestBody Commande t) {
    	return commandeService.modifier(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/commande?id=5
	@GetMapping(value="")
	public Commande rechercherParId (@RequestParam(value="id") Long id) {
    	return commandeService.rechercherParId(id);
    }
    
	//localhost:8080/projetAL/algeek-api/public/commande/all
	@GetMapping(value="/all")
	public List<Commande> getAll(){
    	return commandeService.getAll();
    }

}
