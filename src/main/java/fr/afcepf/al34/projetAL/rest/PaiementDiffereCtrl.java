package fr.afcepf.al34.projetAL.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al34.projetAL.entity.PaiementDiffere;
import fr.afcepf.al34.projetAL.service.PaiementDiffereService;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/algeek-api/public/paiement" , headers="Accept=application/json")
public class PaiementDiffereCtrl {
	
	@Autowired
	private PaiementDiffereService paiementDiffereService;
	
	//localhost:8080/projetAL/algeek-api/public/paiement/add
	@PostMapping(value="/add")
	public PaiementDiffere ajouter(@RequestBody PaiementDiffere paiementDiffere) {
		return paiementDiffereService.ajouter(paiementDiffere);
	}
	
	//localhost:8080/projetAL/algeek-api/public/paiement
	@DeleteMapping(value="")
	public boolean supprimer (@RequestBody PaiementDiffere paiementDiffere) {
    	return paiementDiffereService.supprimer(paiementDiffere);
    }
    
	//localhost:8080/projetAL/algeek-api/public/paiement
	@PutMapping(value="")
	public PaiementDiffere modifier(@RequestBody PaiementDiffere paiementDiffere) {
    	return paiementDiffereService.modifier(paiementDiffere);
    }
    
	//localhost:8080/projetAL/algeek-api/public/paiement?id=1
	@GetMapping(value="")
	public PaiementDiffere rechercherParId (@RequestParam(value="id") Long id) {
    	return paiementDiffereService.rechercherParId(id);
    }
    
	//localhost:8080/projetAL/algeek-api/public/paiement/all
	@GetMapping(value="/all")
	public List<PaiementDiffere> getAll(){
    	return paiementDiffereService.getAll();
    }

}
