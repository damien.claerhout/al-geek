package fr.afcepf.al34.projetAL.rest;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import fr.afcepf.al34.projetAL.entity.Ville;
import fr.afcepf.al34.projetAL.service.VilleService;


@RestController
@CrossOrigin("*")
@RequestMapping(value="/algeek-api/public/ville" , headers="Accept=application/json")
public class VilleRestCtrl {
	
	@Autowired
	private VilleService villeService;
	



	//localhost:8080/projetAL/algeek-api/public/ville/add
	@PostMapping(value="/add")
	public Ville ajouter(@RequestBody Ville t) {
		return villeService.ajouter(t);
	}

	//localhost:8080/projetAL/algeek-api/public/ville
	@DeleteMapping(value="")
	public boolean supprimer (@RequestBody Ville t) {
    	return villeService.supprimer(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/ville
	@PutMapping(value="")
	public Ville modifier(@RequestBody Ville t) {
    	return villeService.modifier(t);
    }
    
	//localhost:8080/projetAL/algeek-api/public/ville?id=1
	@GetMapping(value="")
	public Ville rechercherParId (@RequestParam(value="id") Long id) {
    	return villeService.rechercherParId(id);
    }
    
	//localhost:8080/projetAL/algeek-api/public/ville/all
	@GetMapping(value="/all")
	public List<Ville> getAll(){
    	return villeService.getAll();
    }

}
