package fr.afcepf.al34.projetAL;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import fr.afcepf.al34.projetAL.dao.CategorieDao;
import fr.afcepf.al34.projetAL.dao.ClientDao;
import fr.afcepf.al34.projetAL.dao.CommandeDao;
import fr.afcepf.al34.projetAL.dao.LigneCommandeDao;
import fr.afcepf.al34.projetAL.dao.ProduitDao;
import fr.afcepf.al34.projetAL.dao.TypeProduitDao;
import fr.afcepf.al34.projetAL.entity.Categorie;

import fr.afcepf.al34.projetAL.entity.TypeProduit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Component
@Profile("initData")
@Getter @Setter @NoArgsConstructor
public class InitCategories {
	
	private final long ID_ALIMENTATION 		= 1;
	private final long ID_BOITIER 			= 2;
	private final long ID_CARTE_GRAPHIQUE 	= 3;
	private final long ID_CARTE_MERE 		= 4;
	private final long ID_CARTE_RESEAU 		= 5;
	private final long ID_CLAVIER 			= 6;
	private final long ID_ECRAN 			= 7;
	private final long ID_ORDI_BUREAU 		= 8;
	private final long ID_ORDI_PORTABLE 	= 9;
	private final long ID_PROCESSEUR		= 10;
	private final long ID_RAM				= 11;
	private final long ID_SOURIS			= 12;
	private final long ID_SSD				= 13;
	private final long ID_VENTIRAD			= 14;
	
	@Autowired
	private CategorieDao catDao;
	
	@Autowired
	private TypeProduitDao typeProduitDao;
	
	@Autowired
	private ClientDao clientDao;
	
	@Autowired
	private ProduitDao produitDao;
	
	@Autowired
	private CommandeDao commandeDao;
	
	@Autowired
	private LigneCommandeDao lignecommandeDao;

	@PostConstruct
	public void init() {
		Categorie root = new Categorie(null, null, null, "Nos produits", null);
		
		Categorie n1A = new Categorie(null, root, null, "Ordinateurs de bureau", type(ID_ORDI_BUREAU));
		Categorie n1B = new Categorie(null, root, null, "Ordinateurs portables", type(ID_ORDI_PORTABLE));
		Categorie n1C = new Categorie(null, root, null, "Composants", null);
		Categorie n1D = new Categorie(null, root, null, "Périphériques", null);
		//Categorie n1E = new Categorie(null, root, null, "Logiciels", null);
		
		List<Categorie> categoriesNiveau1 = new ArrayList<>();
		categoriesNiveau1.add(n1A);
		categoriesNiveau1.add(n1B);
		categoriesNiveau1.add(n1C);
		categoriesNiveau1.add(n1D);
		//categoriesNiveau1.add(n1E);
		
		root.setEnfants(categoriesNiveau1);
		
		List<Categorie> composants = new ArrayList<>();
		composants.add(new Categorie(null, n1C, null, "Boitier",			type(ID_BOITIER)));
		composants.add(new Categorie(null, n1C, null, "Alimentation", 		type(ID_ALIMENTATION)));
		composants.add(new Categorie(null, n1C, null, "Carte mère", 		type(ID_CARTE_MERE)));
		composants.add(new Categorie(null, n1C, null, "Processeur", 		type(ID_PROCESSEUR)));
		composants.add(new Categorie(null, n1C, null, "Carte graphique",	type(ID_CARTE_GRAPHIQUE)));
		composants.add(new Categorie(null, n1C, null, "RAM", 				type(ID_RAM)));
		composants.add(new Categorie(null, n1C, null, "Stockage", 			type(ID_SSD)));
		composants.add(new Categorie(null, n1C, null, "Ventirad", 			type(ID_VENTIRAD)));
		composants.add(new Categorie(null, n1C, null, "Carte réseau",		type(ID_CARTE_RESEAU)));
		n1C.setEnfants(composants);

		
		List<Categorie> peripheriques = new ArrayList<>();
		peripheriques.add(new Categorie(null, n1D, null, "Clavier",			type(ID_CLAVIER)));
		peripheriques.add(new Categorie(null, n1D, null, "Souris", 			type(ID_SOURIS)));
		peripheriques.add(new Categorie(null, n1D, null, "Ecran", 			type(ID_ECRAN)));
		n1D.setEnfants(peripheriques);

		catDao.save(root);
		
		for (Categorie c : categoriesNiveau1) {
			catDao.save(c);
		}
		
		for (Categorie c : composants) {
			catDao.save(c);
		}
		
		for (Categorie c : peripheriques) {
			catDao.save(c);
		}
		
		// Client
//		Client cl1 = new Client("Dupont", "Pierre", "pierre@dupont.fr", "test");
//		clientDao.save(cl1);
//		
//		// Commande et LigneCommande
//		Produit p1 = produitDao.findById(704L).get();
//		LigneCommande lc1 = new LigneCommande(null, 1, p1);
//		List<LigneCommande> listLC = new ArrayList<LigneCommande>();
//		listLC.add(lc1);
//		
//		Commande c1 = new Commande(null, listLC, new Date(), p1.getPrix());
//		c1.setClient(cl1);
//		commandeDao.save(c1);
//		lc1.setCommande(c1);
//		lignecommandeDao.save(lc1);		
		
	}

	private TypeProduit type(long i) {
		return typeProduitDao.findById(i).get();
	}
		
	
}
