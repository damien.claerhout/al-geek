package fr.afcepf.al34.projetAL.delegate;

import java.util.List;

import fr.afcepf.al34.projetAL.entity.Produit;


public interface CatalogueDelegate {
	
	List<Produit> getMeilleuresVentes(int numResultatsMax);

	List<Produit> getNouveautes();
	
	Produit ajouter(Produit t);
	
	boolean supprimer (Produit t);

	Produit modifier(Produit t);
    
	Produit rechercherParId (Long id);
    
	List<Produit> getAll();
}
