package fr.afcepf.al34.projetAL.delegate;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.afcepf.al34.projetAL.entity.Panier;
import fr.afcepf.al34.projetAL.entity.ProduitPanier;

@Service
public class PanierDelegateImpl implements PanierDelegate{
	
	private static final String BASE_URL="http://localhost:8080/projetAL/algeek-api/public/panier";
	
	private RestTemplate restTemplate = new RestTemplate();
	
	public Panier ajoutPanier(ProduitPanier produitPanier) {
		String url = BASE_URL + "/ajout";
		
		return restTemplate.postForObject(url,produitPanier,Panier.class);
	}
	
	public Panier retirerProduitDuPanier(ProduitPanier produitPanier) {
		System.out.println(" *** DLGT : tentative de retrait de " + produitPanier.getProduit().getNom());
		String url = BASE_URL + "/retrait";		
		return restTemplate.postForObject(url,produitPanier,Panier.class);
	}

}
