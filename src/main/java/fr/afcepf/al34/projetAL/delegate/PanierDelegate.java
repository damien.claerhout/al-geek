package fr.afcepf.al34.projetAL.delegate;

import fr.afcepf.al34.projetAL.entity.Panier;
import fr.afcepf.al34.projetAL.entity.ProduitPanier;

public interface PanierDelegate {
	
	public Panier ajoutPanier(ProduitPanier produitPanier);
	
	public Panier retirerProduitDuPanier(ProduitPanier produitPanier);

}
