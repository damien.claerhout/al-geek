package fr.afcepf.al34.projetAL.delegate;

import java.util.List;


import fr.afcepf.al34.projetAL.entity.Categorie;


public interface CategorieDelegate {
	
	Categorie getRootCategorie();
	
	Categorie ajouter(Categorie t);
	
	boolean supprimer (Categorie t);

	Categorie modifier(Categorie t);
    
	Categorie rechercherParId (Long id);
    
	List<Categorie> getAll();
	
}
