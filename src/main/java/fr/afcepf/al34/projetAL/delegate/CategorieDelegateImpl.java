package fr.afcepf.al34.projetAL.delegate;


import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.afcepf.al34.projetAL.entity.Categorie;



@Service
public class CategorieDelegateImpl  implements CategorieDelegate {
	
	private static final String BASE_URL="http://localhost:8080/projetAL/algeek-api/public/categorie";
	
	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public Categorie getRootCategorie() {
		
		String url = BASE_URL + "/root";
		return restTemplate.getForObject(url, Categorie.class);
	}

	@Override
	public Categorie ajouter(Categorie t) {
		
		String url = BASE_URL + "/add";
		return restTemplate.postForObject(url, t, Categorie.class);
	}

	@Override
	public boolean supprimer(Categorie t) {
		try {
			
			String url= BASE_URL;
			restTemplate.delete(url);
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public Categorie modifier(Categorie t) {
		
		restTemplate.put(BASE_URL, t);
		return t;
	}

	@Override
	public Categorie rechercherParId(Long id) {
		
		String url = BASE_URL + "?id=" + id;
		return restTemplate.getForObject(url, Categorie.class);
	}

	@Override
	public List<Categorie> getAll() {
		String url = BASE_URL + "/all";
		Categorie[] all = restTemplate.getForObject(url, Categorie[].class);
		return Arrays.asList(all);
	}
	
}
