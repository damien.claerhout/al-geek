package fr.afcepf.al34.projetAL.delegate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.afcepf.al34.projetAL.entity.Produit;

@Service
public class TriProduitsDelegateImpl implements TriProduitsDelegate {

	
	private static final String BASE_URL="http://localhost:8080/projetAL/algeek-api/public/tri";
	
	private RestTemplate restTemplate = new RestTemplate();
	
	@Override
	public List<Produit> trierParMarqueCroissant(List<Produit> produits) {
		String url = BASE_URL + "/marque-croissant";
		Produit[] produitsTries = restTemplate.postForObject(url, produits,  Produit[].class);
		return new ArrayList<Produit>(Arrays.asList(produitsTries));
	}

	@Override
	public List<Produit> trierParMarqueDecroissant(List<Produit> produits) {
		String url = BASE_URL + "/marque-decroissant";
		Produit[] produitsTries = restTemplate.postForObject(url, produits,  Produit[].class);
		return new ArrayList<Produit>(Arrays.asList(produitsTries));
	}

	@Override
	public List<Produit> trierParPrixCroissant(List<Produit> produits) {
		String url = BASE_URL + "/prix-croissant";
		Produit[] produitsTries = restTemplate.postForObject(url, produits,  Produit[].class);
		return new ArrayList<Produit>(Arrays.asList(produitsTries));
	}

	@Override
	public List<Produit> trierParPrixDecroissant(List<Produit> produits) {
		String url = BASE_URL + "/prix-decroissant";
		Produit[] produitsTries = restTemplate.postForObject(url, produits,  Produit[].class);
		return new ArrayList<Produit>(Arrays.asList(produitsTries));
	}

	@Override
	public List<Produit> trierParNomAlphabetique(List<Produit> produits) {
		String url = BASE_URL + "/alphabetique";
		Produit[] produitsTries = restTemplate.postForObject(url, produits,  Produit[].class);
		return new ArrayList<Produit>(Arrays.asList(produitsTries));
	}

}
