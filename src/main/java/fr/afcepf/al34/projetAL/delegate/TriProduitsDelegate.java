package fr.afcepf.al34.projetAL.delegate;

import java.util.List;

import fr.afcepf.al34.projetAL.entity.Produit;

public interface TriProduitsDelegate {
	
	List<Produit> trierParMarqueCroissant(List<Produit> produits);
	
	List<Produit> trierParMarqueDecroissant(List<Produit> produits);	
	
	List<Produit> trierParPrixCroissant(List<Produit> produits);	
	
	List<Produit> trierParPrixDecroissant(List<Produit> produits);	
	
	List<Produit> trierParNomAlphabetique(List<Produit> produits);	

}
