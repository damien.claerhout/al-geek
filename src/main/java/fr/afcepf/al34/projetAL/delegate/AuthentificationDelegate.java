package fr.afcepf.al34.projetAL.delegate;


import fr.afcepf.al34.projetAL.entity.CredentialsInit;
import fr.afcepf.al34.projetAL.service.authentification.exception.AuthentificationException;


public interface AuthentificationDelegate {
	
	CredentialsInit initializeCredentials(CredentialsInit  credentialsInit) throws AuthentificationException;
	
	boolean authentificate(CredentialsInit  credentialsInit) throws AuthentificationException;
}
