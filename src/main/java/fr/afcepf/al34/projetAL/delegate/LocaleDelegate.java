package fr.afcepf.al34.projetAL.delegate;


public interface LocaleDelegate {
	
	String getLocaleString(String cle);
	
}
