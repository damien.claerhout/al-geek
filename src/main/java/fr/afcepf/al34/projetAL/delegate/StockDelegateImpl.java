package fr.afcepf.al34.projetAL.delegate;


import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.entity.Stock;


@Service
public class StockDelegateImpl implements StockDelegate {
	
	private static final String BASE_URL="http://localhost:8080/projetAL/algeek-api/public/stock";
	
	private RestTemplate restTemplate = new RestTemplate();


	@Override
	public List<Stock> getByTypeProduit(Long typeProduitId) {
		
		String url = BASE_URL + "/get-by-type?id=" + typeProduitId;
		Stock[] all = restTemplate.getForObject(url, Stock[].class);
		return Arrays.asList(all);
		
	}
	
	@Override
	public List<Stock> getByQuantiteDispo(int quantite) {
		String url = BASE_URL + "/get-by-quantite?quantite=" + quantite;		
		Stock[] all = restTemplate.getForObject(url, Stock[].class);
		return Arrays.asList(all);
	}
	
	@Override
	public Stock getByProduit(Produit produit) {
		String url = BASE_URL + "/get-by-produit";
		return restTemplate.postForObject(url, produit, Stock.class);
	}
	
	@Override
	public Stock ajouter(Stock t) {
		
		String url = BASE_URL + "/add";
		return restTemplate.postForObject(url, t, Stock.class);
	}

	@Override
	public boolean supprimer(Stock t) {
		
		try {
			
			String url= BASE_URL;
			restTemplate.delete(url);
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public Stock modifier(Stock t) {
		
		restTemplate.put(BASE_URL, t);
		return t;
	}

	@Override
	public Stock rechercherParId(Long id) {
		
		String url = BASE_URL + "?id=" + id;
		return restTemplate.getForObject(url, Stock.class);
	}

	@Override
	public List<Stock> getAll() {
		String url = BASE_URL + "/all";
		Stock[] all = restTemplate.getForObject(url, Stock[].class);
		return Arrays.asList(all);
	}


}
