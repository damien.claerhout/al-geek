package fr.afcepf.al34.projetAL.delegate;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.entity.TypeProduit;

@Service
public class ProduitDelegateImpl implements ProduitDelegate {
	
	private static final String BASE_URL="http://localhost:8080/projetAL/algeek-api/public/produit";
	
	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public List<TypeProduit> getTousTypesProduits() {
		
		String url = BASE_URL + "/all-types";
		TypeProduit[] allTypes = restTemplate.getForObject(url, TypeProduit[].class);
		return Arrays.asList(allTypes);
	}

	@Override
	public Produit getProduitAvecCaracteristiques(Long id) {
		
		String url = BASE_URL + "/caracteristique?id=" + id;
		return restTemplate.getForObject(url, Produit.class);
	}

	@Override
	public List<Produit> getProduitsParType(Long id, boolean chargerCaracteristiques) {
		
		String url = BASE_URL + "/type?id=" + id +"&chargerCaracteristiques=" + chargerCaracteristiques;
		Produit[] productsByType = restTemplate.getForObject(url, Produit[].class);
		return Arrays.asList(productsByType);
	}

	@Override
	public List<Produit> filtrerUsageOccasionnel(List<Produit> listP, String typeOrdi) {
		String url = BASE_URL + "/filtrer-usage-occasionnel?typeOrdi=" + typeOrdi;
		Produit[] produitsUsageOccasionnel= restTemplate.postForObject(url, listP, Produit[].class);
		return Arrays.asList(produitsUsageOccasionnel);
	}

	@Override
	public List<Produit> filtrerUsageRegulier(List<Produit> listP, String typeOrdi) {
		String url = BASE_URL + "/filtrer-usage-regulier?typeOrdi=" + typeOrdi;
		Produit[] produitsUsageRegulier = restTemplate.postForObject(url, listP, Produit[].class);
		return Arrays.asList(produitsUsageRegulier);
	}

	@Override
	public List<Produit> filtrerUsageIntensif(List<Produit> listP, String typeOrdi) {
		String url = BASE_URL + "/filtrer-usage-intensif?typeOrdi=" + typeOrdi;
		Produit[] produitsUsageIntensif = restTemplate.postForObject(url, listP, Produit[].class);
		return Arrays.asList(produitsUsageIntensif);
	}


	@Override
	public List<Produit> trierMoinsCherAuPlusCher(Long idType) {
		
		String url = BASE_URL + "/tri-du-plus-cher-au-moins-cher?idType=" + idType;
		Produit[] produitsDuPlusCherAuMoinsCher = restTemplate.getForObject(url, Produit[].class);
		return Arrays.asList(produitsDuPlusCherAuMoinsCher);
	}

	@Override
	public Produit ajouter(Produit t) {
		
		String url = BASE_URL + "/add";
		return restTemplate.postForObject(url, t, Produit.class);
	}

	@Override
	public boolean supprimer(Produit t) {
		
		try {
			
			String url= BASE_URL;
			restTemplate.delete(url);
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	

	@Override
	public Produit modifier(Produit t) {
		
		restTemplate.put(BASE_URL, t);
		return t;
	}

	@Override
	public Produit rechercherParId(Long id) {
		
		String url = BASE_URL + "?id=" + id;
		return restTemplate.getForObject(url, Produit.class);
	}

	@Override
	public List<Produit> getAll() {
		
		String url = BASE_URL + "/all";
		Produit[] all = restTemplate.getForObject(url, Produit[].class);
		return Arrays.asList(all);
	}
	
}
