package fr.afcepf.al34.projetAL.delegate;

import java.util.List;

import fr.afcepf.al34.projetAL.entity.Client;
import fr.afcepf.al34.projetAL.entity.LoginRequest;

public interface ClientDelegate {
	
	Client doConnecter(LoginRequest loginRequest);
	
	Client findByEmail(String email);
	
	Client ajouter(Client t);
	
	boolean supprimer (Client t);

	Client modifier(Client t);
    
	Client rechercherParId (Long id);
    
	List<Client> getAll();
}
