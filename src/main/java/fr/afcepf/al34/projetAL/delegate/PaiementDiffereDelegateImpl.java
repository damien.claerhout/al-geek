package fr.afcepf.al34.projetAL.delegate;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.afcepf.al34.projetAL.entity.PaiementDiffere;

@Service
public class PaiementDiffereDelegateImpl implements PaiementDiffereDelegate {
	
	// URL qui pointe sur le service déployé sur AWS
	//private static final String BASE_URL_MICROSERVICE="http://35.180.125.66:8080/algeekdif/paiement";
	private static final String BASE_URL_MICROSERVICE="http://localhost:8090/paiement";
	
	private static final String BASE_URL="http://localhost:8080/projetAL/algeek-api/public/paiement";
	//private static final String BASE_URL="http://localhost:8080/projetAL/algeek-api/public/paiement";
		
	private RestTemplate restTemplate = new RestTemplate();
	
	
	public double findPaiementByMensualite(double somme, int mois) {
		String url = BASE_URL_MICROSERVICE +"?a="+ somme +"&b="+ mois;
				return restTemplate.getForObject(url, Double.class);
	}


	@Override
	public PaiementDiffere ajouter(PaiementDiffere t) {
		String url = BASE_URL + "/add";
		return restTemplate.postForObject(url, t, PaiementDiffere.class);
	}


	@Override
	public boolean supprimer(PaiementDiffere t) {
		try {
			
			String url= BASE_URL;
			restTemplate.delete(url);
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}


	@Override
	public PaiementDiffere modifier(PaiementDiffere t) {
		restTemplate.put(BASE_URL, t);
		return t;
	}


	@Override
	public PaiementDiffere rechercherParId(Long id) {
		
		String url = BASE_URL + "?id=" + id;
		return restTemplate.getForObject(url, PaiementDiffere.class);
	}


	@Override
	public List<PaiementDiffere> getAll() {
		String url = BASE_URL + "/all";
		PaiementDiffere[] all = restTemplate.getForObject(url, PaiementDiffere[].class);
		return Arrays.asList(all);
	}

}
