package fr.afcepf.al34.projetAL.delegate;


import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.afcepf.al34.projetAL.entity.Client;
import fr.afcepf.al34.projetAL.entity.LoginRequest;



@Service
public class ClientDelegateImpl implements ClientDelegate {
	
	private static final String BASE_URL="http://localhost:8080/projetAL/algeek-api/public/client";
	
	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public Client doConnecter(LoginRequest loginRequest) {
		String url = BASE_URL + "/login";
		return restTemplate.postForObject(url, loginRequest, Client.class);
	}
	
	@Override
	public Client findByEmail(String email) {
		String url = BASE_URL + "/get-email";
		return restTemplate.postForObject(url, email, Client.class);
	}
	

	@Override
	public Client ajouter(Client t) {
		
		String url = BASE_URL + "/add";
		return restTemplate.postForObject(url, t, Client.class);
	}

	@Override
	public boolean supprimer(Client t) {
		
		try {
			
			String url= BASE_URL;
			restTemplate.delete(url);
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public Client modifier(Client t) {
		
		restTemplate.put(BASE_URL, t);
		return t;
	}

	@Override
	public Client rechercherParId(Long id) {
		
		String url = BASE_URL + "?id=" + id;
		return restTemplate.getForObject(url, Client.class);
	}

	@Override
	public List<Client> getAll() {
		String url = BASE_URL + "/all";
		Client[] all = restTemplate.getForObject(url, Client[].class);
		return Arrays.asList(all);
	}

}
