package fr.afcepf.al34.projetAL.delegate;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.afcepf.al34.projetAL.dto.DepanneurDto;

@Service
public class DepannageDelegateImpl implements DepannageDelegate {

	// adresse pointant sur le service dépanneur déployer sur AWS
	//private static final String BASE_URL="http://35.181.48.243:8080/algeekrep/";
	//private static final String BASE_URL="http://192.168.33.13:8180/depanneur-api/";
		private static final String BASE_URL="http://localhost:8180/depanneur-api/";

	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public List<DepanneurDto> findDepanneursByVille(String ville) {
		String url = BASE_URL + ville;
		DepanneurDto[] depanneurs = restTemplate.getForObject(url, DepanneurDto[].class);
		return Arrays.asList(depanneurs);
	}
	
	
	public String listDepanneursEnJson(String ville) {
		String url = BASE_URL + ville;
		String listDepanneursJson = restTemplate.getForObject(url, String.class);
		return listDepanneursJson;
	}


}
