package fr.afcepf.al34.projetAL.delegate;

import java.util.Date;
import java.util.List;


import fr.afcepf.al34.projetAL.entity.Commande;

public interface CommandeDelegate  {
	
	Commande validerCommande(Commande commande);
	
	List<Commande> getCommandesPourClient(Long idClient);
	
	List<Commande> getCommandesAtraiter();
	
	List<Commande> getCommandesDuJour(Date date);
	
	Commande ajouter(Commande t);
	
	boolean supprimer (Commande t);

	Commande modifier(Commande t);
    
	Commande rechercherParId (Long id);
    
	List<Commande> getAll();
}
