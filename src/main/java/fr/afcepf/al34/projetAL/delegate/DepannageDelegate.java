package fr.afcepf.al34.projetAL.delegate;

import java.util.List;

import fr.afcepf.al34.projetAL.dto.DepanneurDto;

public interface DepannageDelegate {

	List<DepanneurDto> findDepanneursByVille(String ville);
	
	String listDepanneursEnJson(String ville);

}
