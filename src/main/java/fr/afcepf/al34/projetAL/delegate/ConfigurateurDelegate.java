package fr.afcepf.al34.projetAL.delegate;

import java.util.List;

import fr.afcepf.al34.projetAL.entity.ComparaisonProduits;
import fr.afcepf.al34.projetAL.entity.TypeProduit;

public interface ConfigurateurDelegate {
	
	List<TypeProduit> getTypesComposants();
	
	boolean estCompatibleAvec(ComparaisonProduits comparaisonProduit);
	
}
