package fr.afcepf.al34.projetAL.delegate;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.afcepf.al34.projetAL.entity.Commande;
import fr.afcepf.al34.projetAL.entity.LigneCommande;

@Service
public class LigneCommandeDelegateImpl implements LigneCommandeDelegate {
	
	private static final String BASE_URL="http://localhost:8080/projetAL/algeek-api/public/ligne-commande";
	
	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public LigneCommande ajouter(LigneCommande t) {
		
		String url = BASE_URL + "/add";
		return restTemplate.postForObject(url, t, LigneCommande.class);
	}
	
	@Override
	public List<LigneCommande> getByCommande(Commande commande) {
		String url = BASE_URL + "/get-by-commande?commandeId=" + commande.getId();
		LigneCommande[] all = restTemplate.getForObject(url, LigneCommande[].class);
		return  Arrays.asList(all);
	}

	@Override
	public boolean supprimer(LigneCommande t) {
		
		try {
			
			String url= BASE_URL;
			restTemplate.delete(url);
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public LigneCommande modifier(LigneCommande t) {
		
		restTemplate.put(BASE_URL, t);
		return t;
	}

	@Override
	public LigneCommande rechercherParId(Long id) {
		
		String url = BASE_URL + "?id=" + id;
		return restTemplate.getForObject(url, LigneCommande.class);
	}

	@Override
	public List<LigneCommande> getAll() {
		
		String url = BASE_URL + "/all";
		LigneCommande[] all = restTemplate.getForObject(url, LigneCommande[].class);
		return Arrays.asList(all);
	}


	
}
