package fr.afcepf.al34.projetAL.delegate;


import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.afcepf.al34.projetAL.entity.Commande;


@Service
public class CommandeDelegateImpl  implements CommandeDelegate{
	
	private static final String BASE_URL="http://localhost:8080/projetAL/algeek-api/public/commande";
	
	private RestTemplate restTemplate = new RestTemplate();
	
	@Override
	public Commande validerCommande(Commande commande) {
		String url = BASE_URL + "/valider-commande";
		return restTemplate.postForObject(url, commande, Commande.class);		
		
	}

	@Override
	public List<Commande> getCommandesPourClient(Long idClient) {
		
		String url = BASE_URL + "/client?idClient=" + idClient;
		Commande[] commandesDuClient = restTemplate.getForObject(url, Commande[].class);
		return Arrays.asList(commandesDuClient);
	}
	
	@Override
	public List<Commande> getCommandesAtraiter() {
		String url = BASE_URL + "/commandes-a-traiter";
		Commande[] commandesDuClient = restTemplate.getForObject(url, Commande[].class);
		return Arrays.asList(commandesDuClient);
	}

	@Override
	public List<Commande> getCommandesDuJour(Date date) {
		String url = BASE_URL + "/date-du-jour";
		Commande[] commandeDuJour = restTemplate.postForObject(url, date, Commande[].class);
		return Arrays.asList(commandeDuJour);
	}
	
	@Override
	public Commande ajouter(Commande t) {
		
		String url = BASE_URL + "/add";
		return restTemplate.postForObject(url, t, Commande.class);
	}

	@Override
	public boolean supprimer(Commande t) {
		
		try {
			
			String url= BASE_URL;
			restTemplate.delete(url);
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public Commande modifier(Commande t) {
		
		restTemplate.put(BASE_URL, t);
		return t;
	}

	@Override
	public Commande rechercherParId(Long id) {
		
		String url = BASE_URL + "?id=" + id;
		return restTemplate.getForObject(url, Commande.class);
	}

	@Override
	public List<Commande> getAll() {
		
		String url = BASE_URL + "/all";
		Commande[] all = restTemplate.getForObject(url, Commande[].class);
		return Arrays.asList(all);
	}
	

}
