package fr.afcepf.al34.projetAL.delegate;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class LocaleDelegateImpl implements LocaleDelegate {
	
	private static final String BASE_URL="http://localhost:8080/projetAL/algeek-api/public/locale";
	
	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public String getLocaleString(String cle) {
		
		String url = BASE_URL +"?cle=" + cle;
		return restTemplate.getForObject(url, String.class);
	}
	
}
