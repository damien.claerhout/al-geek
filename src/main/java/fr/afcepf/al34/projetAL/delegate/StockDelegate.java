package fr.afcepf.al34.projetAL.delegate;

import java.util.List;

import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.entity.Stock;


public interface StockDelegate {
	
	List<Stock> getByTypeProduit (Long typeProduitId);
	
	public List<Stock> getByQuantiteDispo(int quantite);
	
	Stock getByProduit (Produit produit);
	
	Stock ajouter(Stock t);
	
	boolean supprimer (Stock t);

	Stock modifier(Stock t);
    
	Stock rechercherParId (Long id);
    
	List<Stock> getAll();
}
