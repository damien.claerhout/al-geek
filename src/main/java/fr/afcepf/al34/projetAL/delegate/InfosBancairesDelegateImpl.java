package fr.afcepf.al34.projetAL.delegate;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.afcepf.al34.projetAL.entity.InfosBancaires;

@Service
public class InfosBancairesDelegateImpl implements InfosBancairesDelegate{
	
	private static final String BASE_URL="http://localhost:8080/projetAL/algeek-api/public/infos-bancaires";
	
	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public InfosBancaires ajouter(InfosBancaires t) {
		
		String url = BASE_URL + "/add";
		return restTemplate.postForObject(url, t, InfosBancaires.class);
	}

	@Override
	public boolean supprimer(InfosBancaires t) {
		
		try {
			
			String url= BASE_URL;
			restTemplate.delete(url);
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public InfosBancaires modifier(InfosBancaires t) {
		
		restTemplate.put(BASE_URL, t);
		return t;
	}

	@Override
	public InfosBancaires rechercherParId(Long id) {
		
		String url = BASE_URL + "?id=" + id;
		return restTemplate.getForObject(url, InfosBancaires.class);
	}

	@Override
	public List<InfosBancaires> getAll() {
		
		String url = BASE_URL + "/all";
		InfosBancaires[] all = restTemplate.getForObject(url, InfosBancaires[].class);
		return Arrays.asList(all);
	}
	

}
