package fr.afcepf.al34.projetAL.delegate;

import java.util.List;

import fr.afcepf.al34.projetAL.entity.Commande;
import fr.afcepf.al34.projetAL.entity.LigneCommande;

public interface LigneCommandeDelegate {
	
	List<LigneCommande> getByCommande(Commande commande);
	
	LigneCommande ajouter(LigneCommande t);
	
	boolean supprimer (LigneCommande t);

	LigneCommande modifier(LigneCommande t);
    
	LigneCommande rechercherParId (Long id);
    
	List<LigneCommande> getAll();

}
