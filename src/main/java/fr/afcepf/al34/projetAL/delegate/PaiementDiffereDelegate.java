package fr.afcepf.al34.projetAL.delegate;

import java.util.List;

import fr.afcepf.al34.projetAL.entity.PaiementDiffere;

public interface PaiementDiffereDelegate {
	
	public double findPaiementByMensualite(double somme, int mois);
	
	PaiementDiffere ajouter(PaiementDiffere t);
	
	boolean supprimer (PaiementDiffere t);

	PaiementDiffere modifier(PaiementDiffere t);
    
	PaiementDiffere rechercherParId (Long id);
    
	List<PaiementDiffere> getAll();

}
