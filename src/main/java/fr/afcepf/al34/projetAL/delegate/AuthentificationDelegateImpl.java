package fr.afcepf.al34.projetAL.delegate;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.afcepf.al34.projetAL.entity.CredentialsInit;
import fr.afcepf.al34.projetAL.service.authentification.exception.AuthentificationException;

@Service
public class AuthentificationDelegateImpl implements AuthentificationDelegate{
	
	private static final String BASE_URL="http://localhost:8080/projetAL/algeek-api/public/authentification";
	

	private RestTemplate restTemplate = new RestTemplate();


	@Override
	public CredentialsInit initializeCredentials(CredentialsInit credentialsInit) throws AuthentificationException {
		String url = BASE_URL +"/init";
		return restTemplate.postForObject(url, credentialsInit, CredentialsInit.class);
		
	}


	@Override
	public boolean authentificate(CredentialsInit credentialsInit) throws AuthentificationException {
	
		return restTemplate.postForObject(BASE_URL, credentialsInit, Boolean.class);
	}
	
	

}
