package fr.afcepf.al34.projetAL.delegate;

import java.util.List;

import fr.afcepf.al34.projetAL.entity.Produit;
import fr.afcepf.al34.projetAL.entity.TypeProduit;


public interface ProduitDelegate {
	
	List<TypeProduit> getTousTypesProduits();
	
	Produit getProduitAvecCaracteristiques(Long id);

	List<Produit> getProduitsParType(Long id, boolean chargerCaracteristiques);
	
	List<Produit> filtrerUsageOccasionnel(List<Produit> listP, String typeOrdi);
	
	List<Produit> filtrerUsageRegulier(List<Produit> listP, String typeOrdi);
	
	List<Produit> filtrerUsageIntensif(List<Produit> listP, String typeOrdi);
	
	List<Produit> trierMoinsCherAuPlusCher(Long idType);
	
	Produit ajouter(Produit t);
	
	boolean supprimer (Produit t);

	Produit modifier(Produit t);
    
	Produit rechercherParId (Long id);
    
	List<Produit> getAll();
}
