package fr.afcepf.al34.projetAL.delegate;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import fr.afcepf.al34.projetAL.entity.Produit;

@Service
public class CatalogueDelegateImpl implements CatalogueDelegate{
	
	private static final String BASE_URL="http://localhost:8080/projetAL/algeek-api/public/catalogue";
	

	private RestTemplate restTemplate = new RestTemplate();
	
	public List<Produit> getMeilleuresVentes(int numResultatsMax){
		String url = BASE_URL +"/meilleuresVentes?numResultatsMax=" + numResultatsMax;
		Produit[] tabMeilleuresVentes = restTemplate.getForObject(url, Produit[].class);
		return Arrays.asList(tabMeilleuresVentes);
		
	}
	
	public List<Produit> getNouveautes() {
		String url = BASE_URL + "/nouveautes";
		Produit[] tabNouveautes = restTemplate.getForObject(url, Produit[].class);
		return Arrays.asList(tabNouveautes);
	}
	@Override
	public Produit ajouter(Produit t) {
		String url = BASE_URL + "/add";
		return restTemplate.postForObject(url, t, Produit.class);
	}

	@Override
	public boolean supprimer(Produit t) {
		try {
			
			restTemplate.delete(BASE_URL);
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public Produit modifier(Produit t) {
		
		restTemplate.put(BASE_URL, t);
		return t;
	}

	@Override
	public Produit rechercherParId(Long id) {
		
		String url = BASE_URL + "?id=" + id;
		return restTemplate.getForObject(url, Produit.class);
	}

	@Override
	public List<Produit> getAll() {
		String url = BASE_URL + "/all";
		Produit[] all = restTemplate.getForObject(url, Produit[].class);
		return Arrays.asList(all);
	}
	
	

}
