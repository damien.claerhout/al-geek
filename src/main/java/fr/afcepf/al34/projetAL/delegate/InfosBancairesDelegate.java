package fr.afcepf.al34.projetAL.delegate;

import java.util.List;

import fr.afcepf.al34.projetAL.entity.InfosBancaires;

public interface InfosBancairesDelegate {
	
	InfosBancaires ajouter(InfosBancaires t);
	
	boolean supprimer (InfosBancaires t);

	InfosBancaires modifier(InfosBancaires t);
    
	InfosBancaires rechercherParId (Long id);
    
	List<InfosBancaires> getAll();

}
