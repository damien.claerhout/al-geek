package fr.afcepf.al34.projetAL.delegate;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import fr.afcepf.al34.projetAL.entity.ComparaisonProduits;
import fr.afcepf.al34.projetAL.entity.TypeProduit;


@Service
public class ConfigurateurDelegateImpl implements ConfigurateurDelegate {
	
	private static final String BASE_URL="http://localhost:8080/projetAL/algeek-api/public/configurateur";
	
	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public List<TypeProduit> getTypesComposants() {
		String url = BASE_URL + "/composants";
		TypeProduit[] allTypes = restTemplate.getForObject(url, TypeProduit[].class);
		return Arrays.asList(allTypes);
	}

	@Override
	public boolean estCompatibleAvec (ComparaisonProduits comparaisonProduit) {
		String url = BASE_URL + "/compatibilite";
		
		return restTemplate.postForObject(url, comparaisonProduit, Boolean.class);
	}

	
}
