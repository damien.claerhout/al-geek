// Nous initialisons une liste de marqueurs
var villes = {
	"Paris":{"lat": 48.852969,"lon": 2.349903},
	"Brest":{"lat": 48.383,"lon": -4.500},
	"Quimper":{"lat": 48.000,"lon": -4.100},
	"Bayonne":{"lat": 43.500,"lon": -1.467}
};

var depans = [
	{"id":4,"nomSociete":"PC Assistance","numero":"35","rue":"rue borie","ville":"bordeaux","lat":44.8544571,"lon":-0.5713233},
	{"id":5,"nomSociete":"Jojo PC","numero":"10","rue":"rue borie","ville":"bordeaux","lat":44.8537267,"lon":-0.5703713},
	{"id":6,"nomSociete":"Repar'Ordi","numero":"64","rue":"rue borie","ville":"bordeaux","lat":44.8549182,"lon":-0.5729559}
]

//var depans = document.getElementById("depans");


function initMap() {
	map = new google.maps.Map(document.getElementById("map"), {
		center: new google.maps.LatLng(villes.Bayonne.lat, villes.Bayonne.lon),
		zoom: 5,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		mapTypeControl: true,
		scrollwheel: true,
		mapTypeControlOptions: {
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
		},
		navigationControl: true,
		navigationControlOptions: {
			style: google.maps.NavigationControlStyle.ZOOM_PAN
		}
	});
	// Nous parcourons la liste des villes
	for(ville in villes){
		var marker = new google.maps.Marker({
			// A chaque boucle, la latitude et la longitude sont lues dans le tableau
			position: {lat: villes[ville].lat, lng: villes[ville].lon},
			// On en profite pour ajouter une info-bulle contenant le nom de la ville
			title: ville,
			map: map
		});	
	}
	
	for(depan in depans){
		var marker = new google.maps.Marker({
			// A chaque boucle, la latitude et la longitude sont lues dans le tableau
			position: {lat: depans[depan].lat, lng: depans[depan].lon},
			// On en profite pour ajouter une info-bulle contenant le nom de la ville
			title: depans[depan].nomSociete,
			map: map
		});	
	}
}
window.onload = function(){
	// Fonction d'initialisation qui s'exécute lorsque le DOM est chargé
	initMap(); 
};