//EXEMPLE Avec 4 villes
var villes = {
		"Paris":{"lat": 48.852969,"lon": 2.349903},
		"Brest":{"lat": 48.383,"lon": -4.500},
		"Quimper":{"lat": 48.000,"lon": -4.100},
		"Bayonne":{"lat": 43.500,"lon": -1.467}
};

var adresses = {
		"address1":{"adresse" : "35 rue borie bordeaux"},
		"address2":{"adresse" : "12 rue borie bordeaux"},
		"address3":{"adresse" : "65 rue borie bordeaux"}
};


//var depans = [
//	{"id":4,"nomSociete":"PC Assistance","numero":"35","rue":"rue borie","ville":"bordeaux","lat":44.8544571,"lon":-0.5713233},
//	{"id":5,"nomSociete":"Jojo PC","numero":"10","rue":"rue borie","ville":"bordeaux","lat":44.8537267,"lon":-0.5703713},
//	{"id":6,"nomSociete":"Repar'Ordi","numero":"64","rue":"rue borie","ville":"bordeaux","lat":44.8549182,"lon":-0.5729559}
//]

var depans = document.getElementById("depans");




function initMap() {
	var geocoder = new google.maps.Geocoder();
	var map = new google.maps.Map(document.getElementById("map"), {
		center: new google.maps.LatLng(44.8573809, -0.5776742),
		zoom: 15,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		mapTypeControl: true,
		scrollwheel: true,
		mapTypeControlOptions: {
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
		},
		navigationControl: true,
		navigationControlOptions: {
			style: google.maps.NavigationControlStyle.ZOOM_PAN
		}
	});	
	for(adr in adresses){
		console.log(" * " + adresses[adr].adresse);
		var address = adresses[adr].adresse;
		geocoder.geocode(address);
	};
	for(depan in depans){
		var marker = new google.maps.Marker({
			// A chaque boucle, la latitude et la longitude sont lues dans le tableau
			position: {lat: depans[depan].lat, lng: depans[depan].lon},
			// On en profite pour ajouter une info-bulle contenant le nom de la ville
			title: depans[depan].nomSociete,
			map: map
		});	
	}

}
//Nous parcourons la liste des villes

function geocodeAddress(geocoder, resultsMap) {
	geocoder.geocode({'thead' : adresseeee}, function(results, status) {
		console.log(" ** " + results);
		if (status === 'OK') {
			resultsMap.setCenter(results[0].geometry.location);
			var marker = new google.maps.Marker({
				map: resultsMap,
				position: results[0].geometry.location
			});
		} else {
			alert('Geocode was not successful for the following reason: ' + status);
		}
	});
}


window.onload = function(){
	initMap(); 
};